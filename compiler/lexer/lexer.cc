#include <ctype.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

//ID,NUM
std::unordered_map<std::string, std::string> tokensMap = {
    {"(", "LPAREN"},
    {")", "RPAREN"},
    {"{", "LBRACE"},
    {"}", "RBRACE"},
    {"return", "RETURN"},
    {"if", "IF"},
    {"else", "ELSE"},
    {"while", "WHILE"},
    {"println", "PRINTLN"},
    {"wain", "WAIN"},
    {"=", "BECOMES"},
    {"int", "INT"},
    {"==", "EQ"},
    {"!=", "NE"},
    {"<", "LT"},
    {">", "GT"},
    {"<=", "LE"},
    {">=", "GE"},
    {"+", "PLUS"},
    {"-", "MINUS"},
    {"*", "STAR"},
    {"/", "SLASH"},
    {"%", "PCT"},
    {",", "COMMA"},
    {";", "SEMI"},
    {"new", "NEW"},
    {"delete", "DELETE"},
    {"[", "LBRACK"},
    {"]", "RBRACK"},
    {"&", "AMP"},
    {"//", "COMMENTS"},
    {"MISC", "MISC"},
    {"NULL", "NULL"},
};

bool is_number(const std::string &s)
{
    std::string::const_iterator it = s.begin();
    while (s.end() != it && std::isdigit(*it))
    {
        ++it;
    }
    return !s.empty() && s.end() == it;
}

// returns the type of the token
std::string returnType(std::string str)
{
    // test if it's a token
    if (tokensMap.count(str) > 0)
    {
        return tokensMap[str];
    }

    // test if it's a valid number
    if (is_number(str))
    {
        return "NUM";
    }

    // test if it's a valid id
    if (str.size() == 1 && std::isalpha(str[0]))
    {
        return "ID";
    }

    if (str.size() > 1 && std::isalpha(str[0]))
    {
        std::string::const_iterator it = str.begin();
        ++it;
        while (it != str.end() && (std::isdigit(*it) || std::isalpha(*it)))
        {
            ++it;
        }
        if (!str.empty() && it == str.end())
        {
            return "ID";
        };
    }

    if (str.back() == 32 || str.back() == 9 || str.back() == 10)
    {
        return "MISC";
    }

    return "NOTFOUND";
}

void scanner()
{
    std::vector<std::string> lines;
    std::string line;
    while (std::getline(std::cin, line))
    {
        lines.push_back(line);
    }
    std::vector<std::pair<std::string, std::string>> outputList;
    for (int j = 0; j < lines.size(); ++j)
    {
        line = lines[j];
        std::string token;
        std::string name;
        for (int i = 0; i < line.size(); ++i)
        {
            std::string tmpString = token;
            tmpString.push_back(line[i]);
            std::string newType = returnType(tmpString);

            if (line[i] == '!')
            {
                if (line.size() > (i + 1) && line[i + 1] == '=')
                {
                    if (token != "")
                    {
                        if (name == "NUM")
                        {
                            if (token.size() > 1 && token[0] == '0')
                            {
                                std::cerr << "ERROR: Number error" << std::endl;
                                return;
                            }
                            std::stringstream ss(token);
                            uint64_t x = 0;
                            ss >> x;
                            if (token.size() > 10 || (x > (uint64_t(1) << 31) - 1))
                            {
                                std::cerr << "ERROR: Int out of range" << std::endl;
                                return;
                            }
                        }
                        outputList.push_back(std::pair<std::string, std::string>(name, token));
                    }

                    i++;
                    tmpString = "!=";
                    newType = returnType(tmpString);
                    token = tmpString;
                    name = newType;
                }
                else
                {
                    std::cerr << "ERROR: Lexer error" << std::endl;
                    return;
                }
            }

            if (name == "NUM" && newType == "ID")
            {
                std::cerr << "ERROR: Lexer error" << std::endl;
                return;
            }

            if (newType == "COMMENTS")
            {
                break;
            }
            else if (newType != "NOTFOUND" && newType != "MISC")
            {
                token = tmpString;
                name = newType;

                // std::cout << "token" << tmpString << "." << std::endl;
                // std::cout << "type" << newType << "." << std::endl;

                if (i == line.size() - 1)
                {
                    if (name == "NUM")
                    {
                        if (token.size() > 1 && token[0] == '0')
                        {
                            std::cerr << "ERROR: Number error" << std::endl;
                            return;
                        }
                        std::stringstream ss(token);
                        uint64_t x = 0;
                        ss >> x;
                        if (token.size() > 10 || (x > (uint64_t(1) << 31) - 1))
                        {
                            std::cerr << "ERROR: Int out of range" << std::endl;
                            return;
                        }
                    }
                    outputList.push_back(std::pair<std::string, std::string>(name, token));
                }
                continue;
            }
            else if (token != "")
            {
                if (newType != "MISC")
                {
                    i--;
                }
                if (name == "NUM")
                {
                    if (token.size() > 1 && token[0] == '0')
                    {
                        std::cerr << "ERROR: Number error" << std::endl;
                        return;
                    }
                    std::stringstream ss(token);
                    uint64_t x = 0;
                    ss >> x;
                    if (token.size() > 10 || (x > (uint64_t(1) << 31) - 1))
                    {
                        std::cerr << "ERROR: Int out of range" << std::endl;
                        return;
                    }
                }
                outputList.push_back(std::pair<std::string, std::string>(name, token));
                token = "";
            }
            else if (newType != "MISC")
            {
                std::cerr << "ERROR: input invalid" << std::endl;
                return;
            }
        }
    }

    for (int i = 0; i < outputList.size(); ++i)
    {
        std::cout << outputList[i].first << " " << outputList[i].second << std::endl;
    }

    return;
}

int main()
{
    scanner();
}