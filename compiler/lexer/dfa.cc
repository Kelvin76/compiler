#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>

struct dfa
{
    std::string initSt;
    std::string curSt;
    std::unordered_map<std::string, std::string> finalSt;
    std::unordered_map<std::string, std::string> trans;
};

void dfa()
{
    struct dfa d;
    std::string line;
    int numberOfLines;

    // get rid of the first two parts
    for (int i = 0; i < 2; ++i)
    {
        std::stringstream s;
        std::getline(std::cin, line);
        s << line;
        s >> numberOfLines;
        while (numberOfLines-- > 0)
        {
            std::getline(std::cin, line);
        }
    }

    std::getline(std::cin, d.initSt);

    // get the final states
    std::getline(std::cin, line);
    {
        std::stringstream ss;
        ss << line;
        ss >> numberOfLines;
    }
    while (numberOfLines-- > 0 && std::getline(std::cin, line))
    {
        d.finalSt[line] = true;
    }

    // receive the transitions of the DFA
    std::getline(std::cin, line);
    {
        std::stringstream ss;
        ss << line;
        ss >> numberOfLines;
    }
    while (numberOfLines-- > 0 && std::getline(std::cin, line))
    {
        std::istringstream iss(line);
        std::vector<std::string> part;
        for (std::string s; iss >> s;)
        {
            part.push_back(s);
        }
        d.trans[part[0] + part[1]] = part[2];
    }

    std::vector<std::string> answer;

    // process the inputs
    while (std::getline(std::cin, line))
    {
        // initialize
        d.curSt = d.initSt;
        bool found = true;

        std::istringstream iss(line);
        std::vector<std::string> part;
        for (std::string s; iss >> s;)
        {
            std::string instr = d.curSt + s;
            if (d.trans.count(instr) != 1)
            {
                found = false;
                break;
            }
            d.curSt = d.trans.at(instr);
        }

        if (!found)
        {
            answer.push_back("false");
        }
        else
        {
            answer.push_back(d.finalSt.count(d.curSt) ? "true" : "false");
        }
    }

    for (auto i : answer)
    {
        std::cout << i << std::endl;
    }

    return;
}

int main()
{
    dfa();
    return 0;
}
