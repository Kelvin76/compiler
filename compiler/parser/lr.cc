// reads in a lr file and print the tree

#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

std::string EMPTY = "******";

struct lr {
  std::string initSt;
  std::string accept;
  std::string acceptRHS;
  std::vector<std::string> prod;
  std::unordered_map<std::string, int> shift;
  std::unordered_map<std::string, int> reduce;
};

void lr1() {
  struct lr l;
  std::string line;
  int numberOfLines, numberOfStates, numberOfTransitions;
  std::vector<std::string> symbols;

  std::getline(std::cin, line);
  {
    std::stringstream ss;
    ss << line;
    ss >> numberOfLines;
  }
  while (numberOfLines-- > 0 && std::getline(std::cin, line)) {
    symbols.push_back(line);
  }

  // get rid of the second parts
  // since it's guranteed to be correct

  std::getline(std::cin, line);
  {
    std::stringstream s;
    s << line;
    s >> numberOfLines;
  }
  while (numberOfLines-- > 0) {
    std::getline(std::cin, line);
  }

  std::getline(std::cin, l.initSt);

  // get the production rules and accept final state
  std::getline(std::cin, line);
  {
    std::stringstream ss;
    ss << line;
    ss >> numberOfLines;
  }
  while (numberOfLines-- > 0 && std::getline(std::cin, line)) {
    std::string key = line.substr(0, line.find(" "));
    std::string val = line.substr(line.find(" ") + 1);

    // if this production rule's RHS is empty
    if (line.find(" ") == std::string::npos) {
      l.prod.push_back(key);
      l.prod.push_back(EMPTY);
    } else {
      l.prod.push_back(key);
      l.prod.push_back(line);
      if (val.find("BOF") != std::string::npos &&
          val.find("EOF") != std::string::npos) {
        l.accept = line;
        l.acceptRHS = val;
      }
    }
  }

  std::vector<std::string> answer;

  std::getline(std::cin, line);
  numberOfStates = std::stoi(line);
  std::getline(std::cin, line);
  numberOfTransitions = std::stoi(line);

  // get the transitions
  while (numberOfTransitions-- > 0 && std::getline(std::cin, line)) {
    std::istringstream iss(line);
    std::vector<std::string> part;
    for (std::string s; iss >> s;) {
      part.push_back(s);
    }
    std::string key = part[0] + part[1];
    int rule = std::stoi(part[3]);
    if (part[2] == "reduce") {
      l.reduce[key] = rule;
    } else if (part[2] == "shift") {
      l.shift[key] = rule;
    } else {
      // should not happend if the input rules are correct
      std::cerr << ("ERROR: at least one rule is wrong");
    }
  }

  // store the input in input
  std::vector<std::string> input;

  while (std::getline(std::cin, line)) {
    std::istringstream iss(line);
    std::vector<std::string> part;
    for (std::string s; iss >> s;) {
      input.push_back(s);
    }
  }

  // initialize
  std::vector<std::string> lrSequence;
  std::vector<int> stateStack;
  std::vector<std::string> symbolStack;
  std::string key;

  // process the input
  std::string token, A;
  bool found = false;
  int curSt = 0, prodSt = 0;
  stateStack.push_back(curSt);
  token = input[0];
  key = std::to_string(curSt) + token;
  curSt = l.shift[key];

  symbolStack.push_back(token);
  stateStack.push_back(curSt);

  for (int i = 1; i < input.size(); ++i) {
    found = false;
    token = input[i];

    // try to find a reduce rule
    for (auto sym : symbols) {
      key = std::to_string(stateStack.back()) + sym;
      if (l.reduce.count(key) != 0) {
        std::string str = l.prod[2 * l.reduce[key] + 1];
        if (str == EMPTY ||
            str.substr(str.find_last_of(" ") + 1) == symbolStack.back()) {
          if (l.shift.count(std::to_string(stateStack.back()) + token) == 0) {
            found = true;
          }
          break;
        }
      }
    }

    // reduce
    while (found) {

      found = false;

      std::string str = l.prod[2 * l.reduce[key] + 1];
      prodSt = 2 * l.reduce[key];
      A = l.prod[prodSt];

      if (str != EMPTY) {
        lrSequence.push_back(l.prod[prodSt + 1]);
        while (str.substr(str.find_last_of(" ") + 1) == symbolStack.back()) {
          str = str.substr(0, str.find_last_of(" "));
          symbolStack.pop_back();
          stateStack.pop_back();
        }
      } else {
        lrSequence.push_back(l.prod[prodSt]);
      }

      key = std::to_string(stateStack.back()) + A;
      if (l.reduce.count(key) != 0) {
        curSt = l.reduce[key];
      } else {
        curSt = l.shift[key];
      }
      symbolStack.push_back(A);
      stateStack.push_back(curSt);
      key = std::to_string(stateStack.back()) + A;

      for (auto sym : symbols) {
        key = std::to_string(stateStack.back()) + sym;
        if (l.reduce.count(key) != 0) {
          std::string str = l.prod[2 * l.reduce[key] + 1];
          if (str == EMPTY ||
              str.substr(str.find_last_of(" ") + 1) == symbolStack.back()) {
            if (l.shift.count(std::to_string(stateStack.back()) + token) == 0) {
              found = true;
            }
            break;
          }
        }
      }
    }

    // shift
    symbolStack.push_back(token);
    key = std::to_string(stateStack.back()) + token;
    if (l.shift.count(key) != 0) {
      curSt = l.shift[key];
      stateStack.push_back(curSt);
    } else {
      std::cerr << "ERROR at " << i + 1 << std::endl;
      return;
    }
  }

  std::string symbolStr = symbolStack[0];
  for (int i = 1; i < symbolStack.size(); ++i) {
    symbolStr += " " + symbolStack[i];
  }
  if (symbolStr != l.acceptRHS) {
    std::cerr << "ERROR: at " << l.acceptRHS << std::endl;
    return;
  }

  lrSequence.push_back(l.accept);

  // print
  for (auto i : lrSequence) {
    std::cout << i << std::endl;
  }

  return;
}

int main() {
  lr1();
  return 0;
}
