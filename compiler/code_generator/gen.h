#ifndef GEN_H
#define GEN_H
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

// for the production rules that has nothing on its RHS
std::string EMPTY = "********EMPTY";

// ----------------------  Exception  ------------------
// An exception class thrown when an error is encountered while scanning.
class ScanningFailure
{
  std::string message;

public:
  ScanningFailure(std::string message);

  // Returns the message associated with the exception.
  const std::string &what() const;
};

// tE wrapps the function to throw an exception
void tE(std::string exception)
{
  throw ScanningFailure(exception);
}

// ----------------------  Tree  ---------------------
// Class tree for building the parse tree
class tree
{
public:
  std::string value;
  std::vector<tree *> children;
  std::string varType;

  tree();
  ~tree();
  tree(std::string value);
  void printTree();
  void deleteNode();

  bool childExists(std::string child);
  bool childExistsAfter(std::string child, int ind);
  tree *getNode(std::string child);
  tree *getNodeAfter(std::string child, int ind);

  void setType(std::string type);
  std::string getType(std::string type);
  void build(std::vector<std::string> &inputs, int &ind, int &parens);

  struct type getdclType();
  struct type getexprType(std::map<std::string, std::string> &vars);
  void checkParams(int &index, std::string &proc, std::vector<std::string> &procedures, std::map<std::string, std::string> &vars);
};

// -------------------- wlp4 -------------------
std::string ROOTNODE = EMPTY + "ROOT";

class Wlp4
{
private:
  class tree root;
  std::string curType;
  int label;

  // < procedures , < variables , types > >
  std::map<std::string, std::map<std::string, std::string>> symTable;
  // < procedures , bool >
  std::map<std::string, bool> procedureParam;
  // < procedures , < types > >
  std::map<std::string, std::vector<std::string>> procedures;
  // < procedures , < variable > >
  std::map<std::string, std::vector<std::string>> proceduresParams;
  // < procedures , label >
  std::map<std::string, std::string> proceduresLabel;

  // < procedures , < variables , location > >
  std::map<std::string, std::map<std::string, int>> locTable;
  // return list. < procedure, ready to return >
  std::map<std::string, bool> readyReturn;
  // mips code
  std::vector<std::string> mipsMainCode;
  std::vector<std::string> mipsCode;

  // available registers
  std::unordered_map<int, bool> avail;

public:
  Wlp4();
  ~Wlp4();
  void setRoot(tree root);

  void buildParseTree();
  void printTree();
  void printSymTable();
  void printMips();

  int availReg();
  void resetAvailReg(int reg);

  // checkError will build a parse tree and fill the symTable
  void checkError();
  void errorTraverse(tree &node, std::string proc);

  // generateMips should be called after checkError,
  // since it will use the result gathered by checkError()
  void generateMips();
  void genTraverse(tree &node, std::string proc);
  void addCode(std::vector<std::string> &codes);
  void addMainCode(std::vector<std::string> &codes);
  std::vector<std::string> code(tree &node, std::string proc);
  std::vector<std::string> statement(tree &node, std::string proc);
  std::vector<std::string> setProlog(tree &node, std::string proc);
  std::vector<std::string> setBody(tree &node, std::string proc);
  std::vector<std::string> setEpilog(tree &node, std::string proc);
  std::string generateLabelTag();
  void pushArgs(std::vector<std::string> &source, tree &node, std::string curFunc, std::string tarFunc, int &index);

  std::vector<std::string> mipsTest(tree &node, std::string proc);
  std::vector<std::string> dclsCode(tree &node, std::string proc);

  bool procExists(std::string proc);
  bool varExists(std::string proc, std::string var);
  std::string getVarType(std::string proc, std::string var);
};

// --------------- type -------------
std::string INT = "int";
std::string INTS = "int*";

struct type
{
  bool hasType;
  std::string var;
  std::string type;
};

// ------ mips helper functions -----
std::string noComment = "****NONE****";

enum opcode
{
  notfound = 0,
  jr,
  jalr,
  add,
  sub,
  slt,
  sltu,
  beq,
  bne,
  lis,
  mflo,
  mfhi,
  mult,
  multu,
  divt,
  divu,
  sw,
  lw,
  word
};

// getOpcode returns the enum for opcodes
opcode getOpcode(std::string const &inString)
{
  if (inString == "jr")
    return jr;
  if (inString == "jalr")
    return jalr;
  if (inString == "add")
    return add;
  if (inString == "sub")
    return sub;
  if (inString == "slt")
    return slt;
  if (inString == "sltu")
    return sltu;
  if (inString == "beq")
    return beq;
  if (inString == "bne")
    return bne;
  if (inString == "lis")
    return lis;
  if (inString == "mflo")
    return mflo;
  if (inString == "mfhi")
    return mfhi;
  if (inString == "mult")
    return mult;
  if (inString == "multu")
    return multu;
  if (inString == "div")
    return divt;
  if (inString == "divu")
    return divu;
  if (inString == "sw")
    return sw;
  if (inString == "lw")
    return lw;
  if (inString == "word")
    return word;
  return notfound;
}

// -------------------- helper struct -------------------
struct consFold
{
  bool isConstant;
  int plus;
  int minus;
  int star;
  int slash;
  int mod;
};

#endif