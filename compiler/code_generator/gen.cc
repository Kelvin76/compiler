#include "gen.h"

// ----------------------------- helper func declaration ------------------------
// returns a string with .word followed by a lebel
std::string mipsWord(std::string label);

// returns a string of code supporting beq and bne with label
std::string branchLabel(std::string rule, int s, int t, std::string label);

// returns a string with the corresponding mips code, parameters: (opcode, first
// register, second register, third register, comment, immediate)
std::string mipsCodeFac(std::string instrID, int a = -1, int b = -1, int c = -1, std::string comment = noComment, int i = -1);

// append vectors
void appendSV(std::vector<std::string> &a, std::vector<std::string> &b);

// wrap lis and .word commands
void addAssignment(std::vector<std::string> &source, int d, int value, std::string comment);

// wraps commands for "sw"
void addStore(std::vector<std::string> &source, int s, int d, int index);

// wraps commands for "lw"
void addLoad(std::vector<std::string> &source, int s, int d, int index);

// wraps commands for initializing frame pointer
void addInitFP(std::vector<std::string> &source, int size, std::string proc);

// push $3 into stack
void addPush(std::vector<std::string> &source);

// pop $3 from stack to $5
void addPop(std::vector<std::string> &source);

// returns a local function call vector
void funcCall(std::vector<std::string> &source, std::string func);

// call function call vector
void fCall(std::vector<std::string> &source, std::string func);

// initilize main function (adding constants)
void initMain(std::vector<std::string> &source);

// if two nodes are the same ( for common subexpression)
bool sameTree(tree &node1, tree &node2);

// if two nodes are constants ( for constant folding )
struct consFold areConstants(tree &node1, tree &node2);

// ------------ wlp4 code generate part -------------

// returns a available register and set that register to not avilable
int Wlp4::availReg()
{
  for (auto &i : avail)
  {
    if (i.second == true)
    {
      i.second = false;
      return i.first;
    }
  }
  return -1;
}

// give back the register and set it to available
void Wlp4::resetAvailReg(int reg)
{
  this->avail[reg] = true;
}

// pushes main code into class member mipsCode
void Wlp4::addMainCode(std::vector<std::string> &codes)
{
  appendSV(this->mipsMainCode, codes);
}

// pushes code into class member mipsCode
void Wlp4::addCode(std::vector<std::string> &codes)
{
  appendSV(this->mipsCode, codes);
}

// generate label tag based on class member label
std::string Wlp4::generateLabelTag()
{
  this->label += 1;
  return "label" + std::to_string(label);
}

// returns a structured code for test rules. Store the result in $3. 1 is true,
// 0 is false.
std::vector<std::string> Wlp4::mipsTest(tree &node, std::string proc)
{
  std::vector<std::string> codeLists, tmp;
  std::string rule = node.children[1]->value;

  tmp = this->code(*node.children[0], proc);
  appendSV(codeLists, tmp);
  addPush(codeLists);

  tmp = this->code(*node.children[2], proc);
  appendSV(codeLists, tmp);
  addPop(codeLists);

  // $5 has the value of the first expr
  // $3 has the value of the second expr

  std::string rule1 = "slt", rule2 = "slt";

  if (node.getexprType(symTable[proc]).type == INT)
  {
    if (rule == "LT")
    {
      // $5 < $3
      codeLists.push_back(mipsCodeFac(rule1, 3, 5, 3, "expr < expr", -1));
    }
    else if (rule == "EQ")
    {
      // !($5 < $3) && !($5 > $3)
      codeLists.push_back(mipsCodeFac(rule1, 6, 5, 3, "expr == expr", -1));
      codeLists.push_back(mipsCodeFac(rule1, 7, 3, 5, "", -1));
      codeLists.push_back(mipsCodeFac("add", 3, 6, 7, "", -1));
      codeLists.push_back(mipsCodeFac("sub", 3, 11, 3, "", -1));
    }
    else if (rule == "NE")
    {
      // $5 > $3 || $5 < $3
      codeLists.push_back(mipsCodeFac(rule1, 6, 5, 3, "expr != expr", -1));
      codeLists.push_back(mipsCodeFac(rule1, 7, 3, 5, "", -1));
      codeLists.push_back(mipsCodeFac("add", 3, 6, 7, "", -1));
    }
    else if (rule == "LE")
    {
      // $5 <= $3 => !($5 > $3)
      codeLists.push_back(mipsCodeFac(rule1, 3, 3, 5, "expr <= expr", -1));
      codeLists.push_back(mipsCodeFac("sub", 3, 11, 3, "", -1));
    }
    else if (rule == "GE")
    {
      // $5 >= $3 => !($5 < $3)
      codeLists.push_back(mipsCodeFac(rule1, 3, 5, 3, "expr >= expr", -1));
      codeLists.push_back(mipsCodeFac("sub", 3, 11, 3, "", -1));
    }
    else if (rule == "GT")
    {
      // $5 > $3 == $3 < $5
      codeLists.push_back(mipsCodeFac(rule1, 3, 3, 5, "expr > expr", -1));
    }
    else
    {
      // should not happend
      tE("ERROR: test rule not recognized: " + rule + " .");
    }
  }
  else
  // pointers
  {
    if (rule == "LT")
    {
      // $5 < $3
      codeLists.push_back(mipsCodeFac(rule2, 3, 5, 3, "expr < expr", -1));
    }
    else if (rule == "EQ")
    {
      // !($5 < $3) && !($5 > $3)
      codeLists.push_back(mipsCodeFac(rule2, 6, 5, 3, "expr == expr", -1));
      codeLists.push_back(mipsCodeFac(rule2, 7, 3, 5, "", -1));
      codeLists.push_back(mipsCodeFac("add", 3, 6, 7, "", -1));
      codeLists.push_back(mipsCodeFac("sub", 3, 11, 3, "", -1));
    }
    else if (rule == "NE")
    {
      // $5 > $3 || $5 < $3
      codeLists.push_back(mipsCodeFac(rule2, 6, 5, 3, "expr != expr", -1));
      codeLists.push_back(mipsCodeFac(rule2, 7, 3, 5, "", -1));
      codeLists.push_back(mipsCodeFac("add", 3, 6, 7, "", -1));
    }
    else if (rule == "LE")
    {
      // $5 <= $3 => !($5 > $3)
      codeLists.push_back(mipsCodeFac(rule2, 3, 3, 5, "expr <= expr", -1));
      codeLists.push_back(mipsCodeFac("sub", 3, 11, 3, "", -1));
    }
    else if (rule == "GE")
    {
      // $5 >= $3 => !($5 < $3)
      codeLists.push_back(mipsCodeFac(rule2, 3, 5, 3, "expr >= expr", -1));
      codeLists.push_back(mipsCodeFac("sub", 3, 11, 3, "", -1));
    }
    else if (rule == "GT")
    {
      // $5 > $3 == $3 < $5
      codeLists.push_back(mipsCodeFac(rule2, 3, 3, 5, "expr > expr", -1));
    }
    else
    {
      // should not happend
      tE("ERROR: test rule not recognized: " + rule + " .");
    }
  }

  return codeLists;
}

void Wlp4::pushArgs(std::vector<std::string> &source, tree &node, std::string curFunc, std::string tarFunc, int &index)
{
  std::vector<std::string> tmp;
  // get index of current parameter
  int ind = this->locTable[tarFunc][this->proceduresParams[tarFunc][index]];

  source.push_back(" ");
  source.push_back(";; " + std::to_string(index));
  source.push_back(";; " + this->proceduresParams[tarFunc][index] + " is " + std::to_string(ind));
  source.push_back(" ");

  index = index + 1;

  // arglist expr, use local variable
  tmp = this->code(*node.children[0], curFunc);
  appendSV(source, tmp);
  addStore(source, 3, 30, ind);

  if (node.children.size() == 3)
  {
    // arglist expr COMMA arglist
    this->pushArgs(source, *node.children[2], curFunc, tarFunc, index);
  }

  return;
}

// returns a vector of mips code.
std::vector<std::string> Wlp4::statement(tree &node, std::string proc)
{
  std::vector<std::string> codeLists, tmp;

  if (node.value == "statements")
  {
    // statements
    // statements statements statement
    if (node.children.size() > 1 && node.children[0]->value == "statements")
    {
      tmp = this->statement(*node.children[0], proc);
      appendSV(codeLists, tmp);
      tmp = this->statement(*node.children[1], proc);
      appendSV(codeLists, tmp);
    }
  }
  else if (node.value == "statement")
  {
    if (node.children.size() >= 4)
    {
      if (node.children[0]->value == "lvalue")
      {
        // statement lvalue BECOMES expr SEMI
        tmp = this->code(*node.children[2], proc);
        appendSV(codeLists, tmp);

        tree &tmpNode = *node.children[0];
        while (tmpNode.value == "lvalue")
        {
          if (tmpNode.children[0]->value == "ID")
          {
            // ID
            std::string id = tmpNode.children[0]->children[0]->value;
            addStore(codeLists, 3, 29, this->locTable[proc][id]);
            break;
          }
          else if (tmpNode.children[0]->value == "STAR")
          {
            // STAR factor
            addPush(codeLists);

            tmp = this->code(tmpNode, proc);
            appendSV(codeLists, tmp);

            addPop(codeLists);
            addStore(codeLists, 5, 3, 0);
            break;
          }
          else
          {
            // LPAREN lvalue RPAREN
            tmpNode = *tmpNode.children[1];
          }
        }
      }
      else if (node.children[0]->value == "IF")
      {
        // statement IF LPAREN test RPAREN LBRACE statements RBRACE ELSE LBRACE
        // statements RBRACE
        std::string falseLabel = this->generateLabelTag();
        std::string endLabel = this->generateLabelTag();

        codeLists.push_back(";; if statement ended at " + endLabel);
        // add test codes
        tmp = this->mipsTest(*node.children[2], proc);
        appendSV(codeLists, tmp);
        // decide on the result of $3 (false then goto false label)
        codeLists.push_back(branchLabel("beq", 3, 0, falseLabel));

        // if true
        codeLists.push_back("  \t\t\t\t;; if true:");

        tmp = this->statement(*node.children[5], proc);
        appendSV(codeLists, tmp);

        codeLists.push_back(branchLabel("beq", 0, 0, endLabel));
        // if false
        codeLists.push_back(falseLabel + ":  \t\t;; if false:");

        tmp = this->statement(*node.children[9], proc);
        appendSV(codeLists, tmp);

        // end
        codeLists.push_back(endLabel + ":  \t\t;; if statement from " +
                            endLabel + " end");
      }
      else if (node.children[0]->value == "WHILE")
      {
        // statement WHILE LPAREN test RPAREN LBRACE statements RBRACE
        std::string label1 = this->generateLabelTag();
        std::string label2 = this->generateLabelTag();

        codeLists.push_back(label1 + ":  \t\t;; while loop of " + label1 +
                            " start");

        // add test code
        tmp = this->mipsTest(*node.children[2], proc);
        appendSV(codeLists, tmp);

        // decide on the result of $3 (false then quit)
        codeLists.push_back(branchLabel("beq", 3, 0, label2));

        // add statements code
        tmp = this->statement(*node.children[5], proc);
        appendSV(codeLists, tmp);

        // end of the structure: always jump back to the test codes
        codeLists.push_back(branchLabel("beq", 0, 0, label1));
        codeLists.push_back(label2 + ":  \t\t;; while loop from " + label1 +
                            " end");
      }
      else if (node.children[0]->value == "PRINTLN")
      {
        // statement PRINTLN LPAREN expr RPAREN SEMI
        codeLists.push_back(";; println");
        tmp = this->code(*node.children[2], proc);
        appendSV(codeLists, tmp);
        funcCall(codeLists, "print");
      }
      else if (node.children[0]->value == "DELETE")
      {
        // statement DELETE LBRACK RBRACK expr SEMI
        codeLists.push_back(";; delete");
        tmp = this->code(*node.children[3], proc);
        appendSV(codeLists, tmp);
        std::string labe = this->generateLabelTag();
        codeLists.push_back("beq $3,$11," + labe);
        funcCall(codeLists, "delete");
        codeLists.push_back(labe + ":");
      }
    }
  }
  return codeLists;
}

// returns a vector of mips code. Always store result in $3.
std::vector<std::string> Wlp4::code(tree &node, std::string proc)
{
  std::vector<std::string> codeLists, tmp;

  if (node.value == "expr")
  {
    if (node.children.size() == 1)
    {
      // expr term
      tmp = this->code(*node.children[0], proc);
      appendSV(codeLists, tmp);
    }
    else if (node.children.size() == 3)
    {
      struct consFold cons = areConstants(*node.children[0], *node.children[2]);
      if (cons.isConstant)
      {
        // Constant folding
        if (node.children[1]->value == "PLUS")
        {
          addAssignment(codeLists, 3, cons.plus, "Constant folding");
        }
        else
        {
          addAssignment(codeLists, 3, cons.minus, "Constant folding");
        }
      }
      else
      {
        // No constant folding
        if (node.children[1]->value == "PLUS")
        {
          // expr expr PLUS term
          tmp = this->code(*node.children[0], proc);
          appendSV(codeLists, tmp);

          if (node.children[0]->varType == INT && node.children[2]->varType == INTS)
          {
            codeLists.push_back(mipsCodeFac("mult", 3, 4, -1, "$5 * 4, cuz address"));
            codeLists.push_back(mipsCodeFac("mflo", 3));
          }

          // If there is a available register
          // this kind of code could be simplified easily
          int reg1 = this->availReg();
          if (reg1 != -1)
          {
            codeLists.push_back(mipsCodeFac("add", reg1, 3, 0, "  Optimize: register allocation"));

            tmp = this->code(*node.children[2], proc);
            appendSV(codeLists, tmp);

            if (node.children[0]->varType == INTS && node.children[2]->varType == INT)
            {
              codeLists.push_back(mipsCodeFac("mult", 3, 4, -1, "$3 * 4, cuz address"));
              codeLists.push_back(mipsCodeFac("mflo", 3));
            }

            codeLists.push_back(mipsCodeFac("add", 3, reg1, 3, "$5 + $3"));

            // reset available table
            this->resetAvailReg(reg1);
          }
          else
          {
            // No avail reg (Normal process)
            addPush(codeLists);

            tmp = this->code(*node.children[2], proc);
            appendSV(codeLists, tmp);

            if (node.children[0]->varType == INTS && node.children[2]->varType == INT)
            {
              codeLists.push_back(mipsCodeFac("mult", 3, 4, -1, "$3 * 4, cuz address"));
              codeLists.push_back(mipsCodeFac("mflo", 3));
            }

            addPop(codeLists);
            codeLists.push_back(mipsCodeFac("add", 3, 5, 3, "$5 + $3"));
          }
        }
        else
        {
          // expr expr MINUS term
          tmp = this->code(*node.children[0], proc);
          appendSV(codeLists, tmp);

          // If there is a available register
          int reg1 = this->availReg();
          if (reg1 != -1)
          {
            codeLists.push_back(mipsCodeFac("add", reg1, 3, 0, "  Optimize: register allocation"));

            tmp = this->code(*node.children[2], proc);
            appendSV(codeLists, tmp);

            if (node.children[0]->varType == INTS)
            {
              std::string labe = this->generateLabelTag();
              if (node.children[2]->varType == INT)
              {
                codeLists.push_back(mipsCodeFac("mult", 3, 4, -1, "$3 * 4, cuz address"));
                codeLists.push_back(mipsCodeFac("mflo", 3));
              }

              codeLists.push_back(mipsCodeFac("sub", 3, reg1, 3, "$5 - $3"));

              if (node.children[2]->varType == INTS)
              {
                codeLists.push_back(mipsCodeFac("div", 3, 4, -1, "address - address"));
                codeLists.push_back(mipsCodeFac("mflo", 3));
              }
            }
            else
            {
              codeLists.push_back(mipsCodeFac("sub", 3, reg1, 3, "$5 - $3"));
            }

            // reset available table
            this->resetAvailReg(reg1);
          }
          else
          {
            // No avail reg (Normal process)
            addPush(codeLists);

            tmp = this->code(*node.children[2], proc);
            appendSV(codeLists, tmp);

            if (node.children[0]->varType == INTS)
            {
              std::string labe = this->generateLabelTag();
              if (node.children[2]->varType == INT)
              {
                codeLists.push_back(mipsCodeFac("mult", 3, 4, -1, "$3 * 4, cuz address"));
                codeLists.push_back(mipsCodeFac("mflo", 3));
              }

              addPop(codeLists);
              codeLists.push_back(mipsCodeFac("sub", 3, 5, 3, "$5 - $3"));

              if (node.children[2]->varType == INTS)
              {
                codeLists.push_back(mipsCodeFac("div", 3, 4, -1, "address - address"));
                codeLists.push_back(mipsCodeFac("mflo", 3));
              }
            }
            else
            {
              addPop(codeLists);
              codeLists.push_back(mipsCodeFac("sub", 3, 5, 3, "$5 - $3"));
            }
          }
        }
      }
    }
  }
  else if (node.value == "term")
  {
    if (node.children.size() == 1)
    {
      // term factor
      tmp = this->code(*node.children[0], proc);
      appendSV(codeLists, tmp);
    }
    else if (node.children.size() == 3)
    {
      // term term STAR factor
      // term term SLASH factor
      // term term PCT factor

      struct consFold cons = areConstants(*node.children[0], *node.children[2]);
      if (cons.isConstant)
      {
        // Constant folding
        if (node.children[1]->value == "STAR")
        {
          addAssignment(codeLists, 3, cons.star, "Constant folding");
        }
        else if (node.children[1]->value == "SLASH")
        {
          addAssignment(codeLists, 3, cons.slash, "Constant folding");
        }
        else
        {
          addAssignment(codeLists, 3, cons.mod, "Constant folding");
        }
      }
      else
      {
        // No constant folding
        tmp = this->code(*node.children[0], proc);
        appendSV(codeLists, tmp);

        // If there is a available register
        int reg1 = this->availReg();
        if (reg1 != -1)
        {
          // instead of push
          codeLists.push_back(mipsCodeFac("add", reg1, 3, 0, "  Optimize: register allocation"));

          tmp = this->code(*node.children[2], proc);
          appendSV(codeLists, tmp);

          if (node.children[1]->value == "STAR")
          {
            codeLists.push_back(mipsCodeFac("mult", reg1, 3, -1, "", -1));
            codeLists.push_back(mipsCodeFac("mflo", 3, -1, -1, "$3 = term * factor", -1));
          }
          else if (node.children[1]->value == "SLASH")
          {
            codeLists.push_back(mipsCodeFac("div", reg1, 3, -1, "$3 = term / factor", -1));
            codeLists.push_back(mipsCodeFac("mflo", 3, -1, -1, "$3 = term * factor", -1));
          }
          else
          {
            codeLists.push_back(mipsCodeFac("div", reg1, 3, -1, "$3 = term % factor", -1));
            codeLists.push_back(mipsCodeFac("mfhi", 3, -1, -1, "$3 = term * factor", -1));
          }

          // reset available table
          this->resetAvailReg(reg1);
        }
        else
        {
          // No avail reg (Normal process)
          addPush(codeLists);

          tmp = this->code(*node.children[2], proc);
          appendSV(codeLists, tmp);

          addPop(codeLists);

          if (node.children[1]->value == "STAR")
          {
            codeLists.push_back(mipsCodeFac("mult", 5, 3, -1, "", -1));
            codeLists.push_back(mipsCodeFac("mflo", 3, -1, -1, "$3 = term * factor", -1));
          }
          else if (node.children[1]->value == "SLASH")
          {
            codeLists.push_back(mipsCodeFac("div", 5, 3, -1, "$3 = term / factor", -1));
            codeLists.push_back(mipsCodeFac("mflo", 3, -1, -1, "$3 = term * factor", -1));
          }
          else
          {
            codeLists.push_back(mipsCodeFac("div", 5, 3, -1, "$3 = term % factor", -1));
            codeLists.push_back(mipsCodeFac("mfhi", 3, -1, -1, "$3 = term * factor", -1));
          }
        }
      }
    }
  }
  else if (node.value == "factor")
  {
    if (node.children.size() == 1)
    {
      // factor ID
      if (node.children[0]->value == "ID" && node.children[0]->children.size() >= 1)
      {
        std::string id = node.children[0]->children[0]->value;
        addLoad(codeLists, 3, 29, this->locTable[proc][id]);
      }
      else if (node.children[0]->value == "NUM" && node.children[0]->children.size() >= 1)
      {
        // factor NUM
        int num = std::stoi(node.children[0]->children[0]->value);
        addAssignment(codeLists, 3, num, "temporary value for \"factor NUM\"");
      }
      else if (node.children[0]->value == "NULL")
      {
        // factor NULL
        codeLists.push_back(mipsCodeFac("add", 3, 0, 11));
      }
    }
    else if (node.children.size() == 2)
    {
      if (node.children[0]->value == "AMP")
      {
        // factor AMP lvalue
        tree &tmpNode = *node.children[1];
        while (tmpNode.value == "lvalue")
        {
          if (tmpNode.children[0]->value == "ID")
          {
            // lvalue ID
            std::string id = tmpNode.children[0]->children[0]->value;
            addAssignment(codeLists, 6, this->locTable[proc][id],
                          "get the offset");
            codeLists.push_back(mipsCodeFac(
                "add", 3, 6, 29, "get the address (offset + frame pointer)"));
            break;
          }
          else if (tmpNode.children[0]->value == "STAR")
          {
            // lvalue STAR factor
            tmp = this->code(*tmpNode.children[1], proc);
            appendSV(codeLists, tmp);
            break;
          }
          else if (tmpNode.children[0]->value == "LPAREN")
          {
            // lvalue LPAREN lvalue RPAREN
            tmpNode = *tmpNode.children[1];
          }
        }
      }
      else
      {
        // factor STAR factor
        this->curType = INTS;
        tmp = this->code(*node.children[1], proc);
        this->curType = INT;
        appendSV(codeLists, tmp);
        addLoad(codeLists, 3, 3, 0);
      }
    }
    else if (node.children.size() == 3)
    {
      if (node.children[0]->value == "LPAREN")
      {
        // factor LPAREN expr RPAREN
        if (node.children[1]->children.size() == 1 && node.children[1]->children[0]->children.size() == 1 && node.children[1]->children[0]->children[0]->value == "factor" && node.children[1]->children[0]->children[0]->children.size() == 3 && node.children[1]->children[0]->children[0]->children[1]->value == "expr")
        {
          auto tmpNode = node.children[1]->children[0]->children[0];
          while (tmpNode->children[1]->children.size() == 1 &&
                 tmpNode->children[1]->children[0]->children.size() == 1 &&
                 tmpNode->children[1]->children[0]->children[0]->value ==
                     "factor" &&
                 tmpNode->children[1]->children[0]->children[0]->children.size() ==
                     3 &&
                 tmpNode->children[1]->children[0]->children[0]->children
                         [1]
                             ->value == "expr")
          {
            tmpNode = tmpNode->children[1]->children[0]->children[0];
          }
          tmp = this->code(*tmpNode, proc);
          appendSV(codeLists, tmp);
        }
        else
        {
          tmp = this->code(*node.children[1], proc);
          appendSV(codeLists, tmp);
        }
      }
      else if (node.children[0]->value == "ID" &&
               node.children[0]->children.size() >= 1)
      {
        // factor ID LPAREN RPAREN (function call)
        std::string label = this->proceduresLabel[node.children[0]->children[0]->value];
        fCall(codeLists, label);
      }
    }
    else if (node.children[0]->value == "ID" && node.children.size() == 4 &&
             node.children[0]->children.size() >= 1)
    {
      // factor ID LPAREN arglist RPAREN (function call)
      std::string func = node.children[0]->children[0]->value;
      std::string label = this->proceduresLabel[func];

      codeLists.push_back(";; function call");

      // push 31 and 29 into stack
      addStore(codeLists, 29, 30, -4);

      codeLists.push_back(";; " + std::to_string(symTable[func].size()));

      addAssignment(codeLists, 6, (symTable[func].size() + 2) * 4, "");
      codeLists.push_back(mipsCodeFac("sub", 30, 30, 6));

      // push the arguments into stack, uses 30, so the parameter's index are positive
      int index = 0;
      this->pushArgs(codeLists, *node.children[2], proc, func, index);

      codeLists.push_back(mipsCodeFac("lis", 10, -1, -1, "$10 <-- func addr", -1));
      codeLists.push_back(mipsWord(label));
      codeLists.push_back(mipsCodeFac("jalr", 10, -1, -1, "call subroutine", -1));

      addAssignment(codeLists, 6, (symTable[func].size() + 2) * 4, "");
      codeLists.push_back(mipsCodeFac("add", 30, 30, 6));
      addLoad(codeLists, 29, 30, -4);
    }
    else if (node.children.size() == 5)
    {
      // factor NEW INT LBRACK expr RBRACK
      codeLists.push_back(";; new");
      tmp = this->code(*node.children[3], proc);
      appendSV(codeLists, tmp);
      funcCall(codeLists, "new");
      codeLists.push_back("bne $3, $0, 1");
      codeLists.push_back("add $3, $11, $0");
    }
  }
  else if (node.value == "lvalue")
  {
    if (node.children.size() == 1)
    {
      // lvalue ID
      std::string id = node.children[0]->children[0]->value;
      addLoad(codeLists, 3, 29, this->locTable[proc][id]);
    }
    else if (node.children.size() == 2)
    {
      // lvalue STAR factor
      this->curType = INTS;
      tmp = this->code(*node.children[1], proc);
      this->curType = INT;
      appendSV(codeLists, tmp);
    }
    else if (node.children.size() == 3)
    {
      // lvalue LPAREN lvalue RPAREN
      if (node.children[1]->children.size() == 3)
      {
        auto tmpNode = node.children[1]->children[1];
        while (tmpNode->children.size() == 3)
        {
          tmpNode = tmpNode->children[1];
        }
        tmp = this->code(*tmpNode, proc);
        appendSV(codeLists, tmp);
      }
      else
      {
        tmp = this->code(*node.children[1], proc);
        appendSV(codeLists, tmp);
      }
    }
  }

  return codeLists;
}

// helper function for setProlog
std::vector<std::string> Wlp4::dclsCode(tree &node, std::string proc)
{
  std::vector<std::string> codeLists;

  if (node.children.size() >= 5 && node.children[0]->value == "dcls")
  {
    std::vector<std::string> tmp = this->dclsCode((*node.children[0]), proc);
    appendSV(codeLists, tmp);
    // dcls dcls dcl BECOMES NUM SEMI
    // dcls dcls dcl BECOMES NULL SEMI
    // dcl type ID

    // id is the name of the variable
    std::string id = node.children[1]->children[1]->children[0]->value;
    int value = 0, index = this->locTable[proc][id];

    // value is the value of the variable
    if (node.children[3]->value == "NUM")
    {
      value = std::stoi(node.children[3]->children[0]->value);
    }
    else if (node.children[3]->value == "NULL")
    {
      value = 1;
    }
    else
    {
      // should never happend
      tE("ERROR: variable type is not recognized.");
    }

    // add command
    if (value == 0)
    {
      addStore(codeLists, 0, 29,
               index); // push value in $0 (#0) for the variable
    }
    else if (value == 1)
    {
      addStore(codeLists, 11, 29,
               index); // push value in $11 (#1) for the variable
    }
    else if (value == 4)
    {
      addStore(codeLists, 4, 29,
               index); // push value in $4 (#4) for the variable
    }
    else if (value == 8)
    {
      addStore(codeLists, 18, 29,
               index); // push value in $18 (#8) for the variable
    }
    else if (value == 12)
    {
      addStore(codeLists, 12, 29,
               index); // push value in $12 (#12) for the variable
    }
    else
    {
      addAssignment(codeLists, 6, value, noComment); // put value in $6
      addStore(codeLists, 6, 29, index);             // push value in $6 for the variable
    }
  }
  return codeLists;
}

// setup the locTable and returns the prolog codes (includes initilization of
// variables, which should be part of body)
std::vector<std::string> Wlp4::setProlog(tree &node, std::string proc)
{

  // set locTable
  {
    int in = 0;
    for (auto &i : this->symTable[proc])
    {
      this->locTable[proc][i.first] = (in * (-4));
      ++in;
    }
  }

  // set prolog
  std::vector<std::string> prolog, declarations;

  // set parameters
  if (proc == "wain")
  {
    prolog.push_back(";; prolog");
    prolog.push_back(";; prolog - declarations of parameters");
    prolog.push_back(".import print \t; import the subroutine print");
    prolog.push_back(".import init \t; import the subroutine init");
    prolog.push_back(".import new \t; import the subroutine new");
    prolog.push_back(".import delete \t; import the subroutine delete");

    prolog.push_back(" ");
    prolog.push_back(";; constants");
    initMain(prolog);

    addInitFP(prolog, symTable[proc].size(), proc);

    addStore(prolog, 1, 29, locTable[proc][proceduresParams[proc][0]]);
    addStore(prolog, 2, 29, locTable[proc][proceduresParams[proc][1]]);

    // init initilization
    prolog.push_back(";; init initilization");
    if (procedures["wain"][0] ==
        INTS)
    { // first is int* => do not change $2 before function call
      funcCall(prolog, "init");
    }
    else
    {
      // first is int => $2 = 0
      prolog.push_back(mipsCodeFac("add", 2, 0, 0, "set 2 to zero"));
      funcCall(prolog, "init");
      addLoad(prolog, 2, 29, locTable[proc][proceduresParams[proc][1]]);
    }
  }
  else
  {
    // other function
    prolog.push_back(" ");
    prolog.push_back(" ");
    prolog.push_back(" ");
    prolog.push_back(" ");
    prolog.push_back(proceduresLabel[proc] + ":   ;; procedure label, function: " + proc);
    prolog.push_back(";; subprocedure");
    if (procedures[proc][0] != EMPTY && proc != "wain")
    {

      // modify locTable
      {
        int in = procedures[proc].size() * 4;
        for (auto &i : locTable[proc])
        {
          this->locTable[proc][i.first] = i.second + in;
        }
      }

      prolog.push_back(mipsCodeFac("add", 29, 30, 0, "init fp"));

      declarations.push_back(";; body - declarations of local variables");
      if (node.children.size() > 1 && node.children[0]->value == "dcls")
      {
        std::vector<std::string> tmp = this->dclsCode(node, proc);
        appendSV(declarations, tmp);
      }
      appendSV(prolog, declarations);

      declarations.push_back(";; body - declarations of local variables");
      addAssignment(prolog, 6, 4 * (symTable[proc].size() - proceduresParams[proc].size() + 3), "");
      prolog.push_back(mipsCodeFac("sub", 30, 30, 6, "init fp"));

      addStore(prolog, 31, 30, 4);
      addStore(prolog, 5, 30, 8);

      declarations.push_back(" ");
      declarations.push_back(";; body");
    }
    else
    {
      prolog.push_back(";; prolog - declarations of parameters");
      addInitFP(prolog, symTable[proc].size(), proc);
    }
  }

  // set local parameters
  if (procedures[proc][0] == EMPTY || proc == "wain")
  {
    declarations.push_back(" ");
    declarations.push_back(";; body");
    declarations.push_back(";; body - declarations of local variables");
    if (node.children.size() > 1 && node.children[0]->value == "dcls")
    {
      std::vector<std::string> tmp = this->dclsCode(node, proc);
      appendSV(declarations, tmp);
    }
    appendSV(prolog, declarations);
  }

  return prolog;
}

std::vector<std::string> Wlp4::setBody(tree &node, std::string proc)
{
  // statements
  std::vector<std::string> codeLists, tmp;
  codeLists.push_back(";; body - statements");

  tmp = this->statement(node, proc);
  appendSV(codeLists, tmp);

  return codeLists;
}

std::vector<std::string> Wlp4::setEpilog(tree &node, std::string proc)
{
  // expr
  std::vector<std::string> codeLists, tmp;

  codeLists.push_back(";; body - return");
  tmp = this->code(node, proc);
  appendSV(codeLists, tmp);

  codeLists.push_back(" ");
  codeLists.push_back(";; epilog");

  if (procedures[proc][0] != EMPTY && proc != "wain")
  {
    addLoad(codeLists, 31, 30, 4);
    addLoad(codeLists, 5, 30, 8);
    codeLists.push_back(mipsCodeFac("add", 30, 29, 0, "pop stack"));
  }
  else
  {
    codeLists.push_back(mipsCodeFac("add", 30, 29, 18, "pop stack"));
    addLoad(codeLists, 31, 30, -4);
  }
  codeLists.push_back(mipsCodeFac("jr", 31, -1, -1, "exit"));

  return codeLists;
}

// helper function for generateMips()
void Wlp4::genTraverse(tree &node, std::string proc)
{
  // applying rules
  std::vector<std::string> tmp;

  // get procedure name
  if (node.value == "procedure")
  {
    if (node.children.size() >= 1 && node.children[1]->children.size() >= 1)
    {
      proc = node.children[1]->children[0]->value;
      this->readyReturn[proc] = false;
      this->proceduresLabel[proc] = this->generateLabelTag();
    }
  }
  else if (node.value == "main")
  {
    proc = "wain";
    this->readyReturn[proc] = false;
    this->proceduresLabel[proc] = this->generateLabelTag();
  }

  // if current node is not under any procedure node, then continue to traverse
  // under a subroutine
  if (this->procExists(proc))
  {
    if (proc == "wain")
    {
      if (node.value == "dcls")
      {
        // prolog
        tmp = this->setProlog(node, proc);
        this->addMainCode(tmp);
        return;
      }
      else if (node.value == "statements")
      {
        // body
        tmp = this->setBody(node, proc);
        this->addMainCode(tmp);
        return;
      }
      else if (node.value == "RETURN")
      {
        readyReturn[proc] = true;
        return;
      }
      else if (readyReturn[proc] && node.value == "expr")
      {
        // epilog
        tmp = this->setEpilog(node, proc);
        this->addMainCode(tmp);
        this->readyReturn[proc] = false;
      }
    }
    else
    {
      if (node.value == "dcls")
      {
        // prolog
        tmp = this->setProlog(node, proc);
        this->addCode(tmp);
        return;
      }
      else if (node.value == "statements")
      {
        // body
        tmp = this->setBody(node, proc);
        this->addCode(tmp);
        return;
      }
      else if (node.value == "RETURN")
      {
        readyReturn[proc] = true;
        return;
      }
      else if (readyReturn[proc] && node.value == "expr")
      {
        // epilog
        tmp = this->setEpilog(node, proc);
        this->addCode(tmp);
        this->readyReturn[proc] = false;
      }
    }
  }

  // traverse children
  if (node.children.size() != 0)
  {
    for (int in = 0; in < node.children.size(); ++in)
    {
      this->genTraverse((*node.children[in]), proc);
    }
  }

  return;
}

// generates mips code
void Wlp4::generateMips() { this->genTraverse(this->root, EMPTY); }

// ------------------ main --------------------
// run is a structured function for try-catch
int run()
{
  Wlp4 cgen;
  try
  {
    // build the parse tree
    cgen.buildParseTree();

    // analysis the types
    cgen.checkError();

    // generate mips code
    cgen.generateMips();

    // print the mips code
    cgen.printMips();
  }
  catch (ScanningFailure &f)
  {
    std::cerr << f.what() << std::endl;
  }

  return 0;
}

///////////////////////////
//     main function     //
///////////////////////////
int main() { return run(); }

// -------------------- WLP4 functions implementations
// --------------------------------

Wlp4::Wlp4()
{
  this->root = tree(ROOTNODE);
  this->label = 0;
  this->curType = INT;

  for (int i = 9; i < 29; ++i)
  {
    this->avail[i] = true; // which is available
  }

  avail[11] = false; // always 1
  avail[12] = false; // always 12
  avail[18] = false; // always 8
}

Wlp4::~Wlp4() {}

void Wlp4::printTree() { root.printTree(); }

void Wlp4::setRoot(tree root) { this->root = root; }

// print the codes stored in vector mipsCode
void Wlp4::printMips()
{
  for (auto it : this->mipsMainCode)
  {
    std::cout << it << std::endl;
  }
  for (auto it : this->mipsCode)
  {
    std::cout << it << std::endl;
  }
}

// print the symbol table with stderr
void Wlp4::printSymTable()
{
  for (auto &proc : procedures)
  {
    if (proc.first == "wain")
    {
      continue;
    }

    std::cerr << proc.first;
    // print parameters
    if (procedures[proc.first][0] != EMPTY)
    {
      for (auto &var : proc.second)
      {
        std::cerr << " " << var;
      }
    }
    std::cerr << std::endl;

    // print local variables
    for (auto &var : symTable[proc.first])
    {
      std::cerr << var.first << " " << var.second << std::endl;
    }
    std::cerr << std::endl;
  }
  {
    std::cerr << "wain";
    for (int i = 0; i < procedures["wain"].size(); ++i)
    {
      std::cerr << " " << procedures["wain"][i];
    }
    std::cerr << std::endl;
    for (auto &var : symTable["wain"])
    {
      std::cerr << var.first << " " << var.second << std::endl;
    }
  }
}

// ----------- wlp4 error checking part -----------
void Wlp4::buildParseTree()
{
  std::vector<std::string> inputs;
  std::string line;

  // store the inputs
  while (std::getline(std::cin, line))
  {
    inputs.push_back(line);
  }

  // build the tree
  int index = 0, parens = 0;

  this->root.build(inputs, index, parens);
}

bool Wlp4::procExists(std::string proc)
{
  return (symTable.count(proc) != 0) ? true : false;
}

bool Wlp4::varExists(std::string proc, std::string var)
{
  if (this->procExists(proc))
  {
    return (symTable[proc].count(var) != 0) ? true : false;
  }
  return false;
}

// need to make sure the variable exists before using this func
std::string Wlp4::getVarType(std::string proc, std::string var)
{
  return symTable[proc][var];
}

// helper recursive function for checkError()
void Wlp4::errorTraverse(tree &node, std::string proc)
{
  // applying rules
  if (node.value == "procedure")
  {
    // if procedure with the same name exists
    if (node.children.size() >= 1 && node.children[1]->children.size() >= 1 &&
        this->procExists(node.children[1]->children[0]->value))
    {
      tE("ERROR: procedure " + node.children[1]->children[0]->value +
         " was already declared.");
    }
    if (node.children.size() >= 1 && node.children[1]->children.size() >= 1)
    {
      proc = node.children[1]->children[0]->value;
      this->procedureParam[proc] = true;
    }
  }
  else if (node.value == "main")
  {
    if (node.value == "main" && node.children.size() >= 5 &&
        node.children[5]->value == "dcl")
    {
      // main INT WAIN LPAREN dcl COMMA dcl RPAREN LBRACE dcls statements RETURN
      // expr SEMI RBRACE
      if (node.children[5]->getdclType().type != INT)
      {
        tE("ERROR: Second argument of wain must be of type INT: got [" + INTS +
           " " + node.children[5]->getdclType().var + "].");
      }
    }
    proc = "wain";
    this->procedureParam[proc] = true;
  }
  else if (node.value == "dcl")
  {
    // variable declaration
    struct type t = node.getdclType();
    // if variable was already declared
    if (this->varExists(proc, t.var))
    {
      tE("ERROR: variable " + t.var + " was already declared.");
    }
    this->symTable[proc][t.var] = t.type;
    if (procedureParam[proc])
    {
      this->procedures[proc].push_back(t.type);
      this->proceduresParams[proc].push_back(t.var);
    }
  }
  else if (node.value == "factor" && node.children.size() == 1)
  {
    // if variable was not initialized
    if (node.childExists("ID"))
    {
      std::string str = node.getNode("ID")->children[0]->value;
      if (!this->varExists(proc, str))
      {
        tE("ERROR: variable " + str + " was not initialized.");
      }
    }
  }
  else if (node.value == "factor" && node.children.size() == 4 &&
           node.children[0]->value == "ID" &&
           node.children[1]->value == "LPAREN")
  {
    std::string str = node.getNode("ID")->children[0]->value;
    if (symTable[proc].count(str) == 0)
    {
      // function call but function was not declared
      if (symTable.count(str) == 0)
      {
        tE("ERROR: function " + str + " was not declared.");
      }
      // function call check signature (number, type)
      int indexP = 0;
      node.getNode("arglist")
          ->checkParams(indexP, proc, procedures[str], symTable[proc]);
      if (indexP + 1 < procedures[str].size())
      {
        tE("ERROR: # of parameters is less. Need " +
           std::to_string(procedures[str].size()) + ", but has " +
           std::to_string(indexP + 1) + ". Procedure:" + str);
      }
    }
    else
    {
      tE("ERROR: Variable " + str + " is not a function.");
    }
  }
  else if (node.value == "factor" && node.children.size() == 3 &&
           node.children[0]->value == "ID" &&
           node.children[1]->value == "LPAREN")
  {
    std::string str = node.getNode("ID")->children[0]->value;
    if (symTable[proc].count(str) == 0)
    {
      // function call but function was not declared
      if (symTable.count(str) == 0)
      {
        tE("ERROR: function " + str + " was not declared.");
      }
      // function call check signature (number, type)
      if (procedures[str][0] != EMPTY)
      {
        tE("ERROR: # of parameters is wrong. Procedure " + str +
           " has no paramter.");
      }
    }
    else
    {
      tE("ERROR: Variable " + str + " is not a function.");
    }
  }
  else if (node.value == "statement" && node.children.size() == 4)
  {
    if (node.children[0]->value == "lvalue")
    {
      // statement lvalue BECOMES expr SEMI
      if (node.children[0]->getexprType(symTable[proc]).type !=
          node.children[2]->getexprType(symTable[proc]).type)
      {
        tE("ERROR: statement has different type on each side. Left:" +
           node.children[0]->getexprType(symTable[proc]).type + ",Right:" +
           node.children[2]->getexprType(symTable[proc]).type + ".");
      }
    }
  }
  else if (node.value == "statement" && node.children.size() == 5)
  {
    if (node.children[0]->value == "PRINTLN")
    {
      // statement PRINTLN LPAREN expr RPAREN SEMI
      if (node.children[2]->getexprType(symTable[proc]).type != INT)
      {
        tE("ERROR: Println needs type INT. But we have:" +
           node.children[2]->getexprType(symTable[proc]).type + ".");
      }
    }
    if (node.children[0]->value == "DELETE")
    {
      // statement DELETE LBRACK RBRACK expr SEMI
      if (node.children[3]->getexprType(symTable[proc]).type != INTS)
      {
        tE("ERROR: Delete needs type INT STAR. But we have:" +
           node.children[3]->getexprType(symTable[proc]).type + ".");
      }
    }
  }
  else if (node.value == "statement" && node.children.size() == 7 &&
           node.children[0]->value == "WHILE")
  {
    // statement WHILE LPAREN test RPAREN LBRACE statements RBRACE
    // if test is well-typed
    if (node.children[2]->value == "test" &&
        node.children[2]->children.size() == 3)
    {
      if (node.children[2]->children[0]->value == "expr" &&
          node.children[2]->children[2]->value == "expr")
      {
        if (node.children[2]->children[0]->getexprType(symTable[proc]).type !=
            node.children[2]->children[2]->getexprType(symTable[proc]).type)
        {
          tE("ERROR: test has different type on each side. Left:" +
             node.children[2]->children[0]->getexprType(symTable[proc]).type +
             ",Right:" +
             node.children[2]->children[2]->getexprType(symTable[proc]).type +
             ".");
        }
      }
    }
  }
  else if (node.value == "statement" && node.children.size() == 11 &&
           node.children[0]->value == "IF")
  {
    // statement IF LPAREN test RPAREN LBRACE statements RBRACE ELSE LBRACE
    // statements RBRACE
    // if test is well-typed
    if (node.children[2]->value == "test" &&
        node.children[2]->children.size() == 3)
    {
      if (node.children[2]->children[0]->value == "expr" &&
          node.children[2]->children[2]->value == "expr")
      {
        if (node.children[2]->children[0]->getexprType(symTable[proc]).type !=
            node.children[2]->children[2]->getexprType(symTable[proc]).type)
        {
          tE("ERROR: test has different type on each side. Left:" +
             node.children[2]->children[0]->getexprType(symTable[proc]).type +
             ",Right:" +
             node.children[2]->children[2]->getexprType(symTable[proc]).type +
             ".");
        }
      }
    }
  }
  else if (node.value == "dcls" && node.children.size() == 5 &&
           node.children[0]->value == "dcls")
  {
    // if test is well-typed
    if (node.children[3]->value == "NUM" &&
        node.children[1]->children.size() >= 1)
    {
      // dcls dcls dcl BECOMES NUM SEMI
      if (node.children[1]->getdclType().type != INT)
      {
        tE("ERROR: cannot assign INT to a pointer. In procedure: " + proc);
      }
    }
    else if (node.children[3]->value == "NULL" &&
             node.children[1]->children.size() >= 1)
    {
      // dcls dcls dcl BECOMES NULL SEMI
      if (node.children[1]->getdclType().type != INTS)
      {
        tE("ERROR: cannot assign NULL to a INT variable. In procedure: " +
           proc);
      }
    }
  }

  // traverse children
  if (node.children.size() != 0)
  {
    for (int in = 0; in < node.children.size(); ++in)
    {
      if (((in + 1) < node.children.size()) &&
          node.children[in]->value == "LBRACE" &&
          node.children[in + 1]->value == "dcls")
      {
        if (symTable[proc].size() == 0)
        {
          procedures[proc].push_back(EMPTY);
        }
        this->procedureParam[proc] = false;
      }
      else if (((in + 1) < node.children.size()) &&
               node.children[in]->value == "RETURN" &&
               node.children[in + 1]->value == "expr")
      {
        // return type should be INT
        if (node.children[in + 1]->getexprType(symTable[proc]).type != INT)
        {
          tE("ERROR: Return type should always be INT. But procedure " + proc +
             " is not.");
        }
      }
      this->errorTraverse((*node.children[in]), proc);
    }
  }

  return;
}

// checkErrors implementation
void Wlp4::checkError()
{
  // pre-order traverse each procedure
  this->errorTraverse(this->root, EMPTY);
}

// ------------------------- tree functions implementation
// --------------------------

tree::tree() {}

tree::tree(std::string value) { this->value = value; }

void tree::deleteNode()
{
  for (int i = 0; i < children.size(); ++i)
  {
    (*children[i]).deleteNode();
    delete children[i];
    children[i] = nullptr;
  }
  children.clear();
  return;
}

tree::~tree()
{
  deleteNode();
  return;
}

void tree::printTree()
{
  std::cout << this->value << std::endl;
  for (int i = 0; i < this->children.size(); ++i)
  {
    (*this->children[i]).printTree();
  }
  return;
}

bool tree::childExists(std::string child)
{
  for (auto it : children)
  {
    if (child == (*it).value)
    {
      return true;
    }
  }
  return false;
}

bool tree::childExistsAfter(std::string child, int ind)
{
  for (; ind < children.size(); ++ind)
  {
    if (child == (*children[ind]).value)
    {
      return true;
    }
  }
  return false;
}

tree *tree::getNode(std::string child)
{
  for (auto it : children)
  {
    if (child == (*it).value)
    {
      return it;
    }
  }
  return NULL;
}

tree *tree::getNodeAfter(std::string child, int ind)
{
  for (; ind < children.size(); ++ind)
  {
    if (child == (*children[ind]).value)
    {
      return children[ind];
    }
  }
  return NULL;
}

void tree::setType(std::string type) { this->varType = type; }

std::string tree::getType(std::string type) { return varType; }

// recursion function for building the tree
void tree::build(std::vector<std::string> &inputs, int &ind, int &parens)
{
  int after = 0;
  bool paren = false;

  // base cases:
  while (ind != inputs.size())
  {
    // special cases
    // get rid of parenthesis when there are too many (not a usual situation)
    if (ind + 7 < inputs.size() && inputs[ind] == "factor LPAREN expr RPAREN")
    {
      while (ind + 7 < inputs.size() &&
             inputs[ind] == "factor LPAREN expr RPAREN" &&
             inputs[ind + 1] == "LPAREN (" && inputs[ind + 2] == "expr term" &&
             inputs[ind + 3] == "term factor" &&
             inputs[ind + 4] == "factor LPAREN expr RPAREN" &&
             inputs[ind + 5] == "LPAREN (" && inputs[ind + 6] == "expr term" &&
             inputs[ind + 7] == "term factor")
      {
        parens++;
        ind += 4;
      }
      paren = true;
    }

    // get the LHS and RHS
    std::string LHS, RHS;
    {
      std::string str = inputs[ind];
      int index = str.find(" ");
      if (index != std::string::npos)
      {
        LHS = str.substr(0, index);
        RHS = str.substr(index + 1);
      }
      else
      {
        LHS = str;
        RHS = EMPTY;
      }
    }

    // special case when this is the root
    if (this->value == ROOTNODE)
    {
      this->value = LHS;
      if (RHS == EMPTY)
      {
        // should not happend
        tE("ERROR: The first rule is not valid");
      }
      else
      {
        std::istringstream iss(RHS);
        for (std::string s; iss >> s;)
        {
          this->children.push_back(new tree(s));
        }
      }
      ++ind;
    }
    else
    {
      // normal cases: has children
      if (this->childExistsAfter(LHS, after))
      {
        this->getNodeAfter(LHS, after)->build(inputs, ind, parens);
        for (; after < children.size(); ++after)
        {
          if (LHS == (*children[after]).value)
          {
            ++after;
            break;
          }
        }
      }
      else if (this->value == LHS && this->children.size() == 0)
      {
        std::istringstream iss(RHS);
        for (std::string s; iss >> s;)
        {
          this->children.push_back(new tree(s));
        }
        ++ind;
      }
      else
      {
        return;
      }
    }

    // get rid of right parenthesis when there are too many (not a usual
    // situation)
    if (ind + 1 < inputs.size() && inputs[ind] == "RPAREN )" &&
        inputs[ind + 1] == "RPAREN )")
    {
      while (ind + 1 < inputs.size() && inputs[ind] == "RPAREN )" &&
             inputs[ind + 1] == "RPAREN )")
      {
        parens--;
        ind += 1;
      }
    }
  }
  if (paren && parens != 0)
  {
    tE("ERROR: parenthesises are not symmetric. Parenthesis: " +
       std::to_string(parens));
  }

  return;
}

// current scope:
// vars: variables in current scope
// proc: current procedure (scope)
// function call:
// procedures: the variables needed for the function call
// index: the index of the parameters
void tree::checkParams(int &index, std::string &proc,
                       std::vector<std::string> &procedures,
                       std::map<std::string, std::string> &vars)
{
  if (this->value == "COMMA")
  {
    ++index;
    if (index >= procedures.size())
    {
      tE("ERROR: # of parameters is wrong (more than needed). Need " +
         std::to_string(procedures.size()) + ", but has " +
         std::to_string(index + 1) + ". Function call is in the procedure:" +
         proc);
    }
    return;
  }

  if (this->value == "expr")
  {
    struct type t = this->getexprType(vars);
    if (procedures[index] != t.type)
    {
      tE("ERROR: parameter type wrong. We need [" + procedures[index] +
         "], but found [" + t.type + "]. Procedure:" + proc);
    }
    return;
  }

  if (this->children.size() != 0)
  {
    for (int in = 0; in < this->children.size(); ++in)
    {
      this->children[in]->checkParams(index, proc, procedures, vars);
    }
  }
}

// --------------------- type functions implementations ----------------------

// typeOp will return the type of two types combined with a operation
struct type typeOp(struct type t1, struct type t2, std::string op)
{

  struct type t;
  if (op == "PLUS")
  {
    if (t1.type == INT && t2.type == INT)
    {
      t.type = INT;
    }
    else if (t1.type == INTS && t2.type == INT)
    {
      t.type = INTS;
    }
    else if (t1.type == INT && t2.type == INTS)
    {
      t.type = INTS;
    }
    else
    {
      tE("ERROR: Addition of multiple pointers is not supported.");
    }
  }
  else if (op == "MINUS")
  {
    if (t1.type == INT && t2.type == INT)
    {
      t.type = INT;
    }
    else if (t1.type == INTS && t2.type == INT)
    {
      t.type = INTS;
    }
    else if (t1.type == INTS && t2.type == INTS)
    {
      t.type = INT;
    }
    else
    {
      tE("ERROR: INT subtract a INT STAR is not supported.");
    }
  }
  else if (op == "STAR" || op == "SLASH" || op == "PCT")
  {
    if (t1.type == INT && t2.type == INT)
    {
      t.type = INT;
    }
    else
    {
      tE("ERROR: Multiplication/Division of pointers is not supported.");
    }
  }
  else
  {
    // should not happend
    tE("ERROR: operation code not recognized.");
  }

  return t;
}

// return the type struct from a dcl node
struct type tree::getdclType()
{
  struct type t;
  if (this->value != "dcl")
  {
    t.hasType = false;
    return t;
  }
  t.hasType = true;

  {
    auto varType = this->getNode("type")->children;
    if (varType.size() == 1)
    {
      t.type = INT;
    }
    else
    {
      t.type = INTS;
    }
  }

  t.var = this->getNode("ID")->children[0]->value;
  return t;
}

// return the type struct from a expr node
struct type tree::getexprType(std::map<std::string, std::string> &vars)
{
  struct type t;

  if (this->value == "expr")
  {
    if (this->children.size() == 1)
    {
      // expr term
      t = this->children[0]->getexprType(vars);
    }
    else if (this->children.size() == 3)
    {
      struct type t1, t2;
      t1 = this->children[0]->getexprType(vars);
      t2 = this->children[2]->getexprType(vars);
      // expr expr PLUS term
      // expr expr MINUS term
      t = typeOp(t1, t2, this->children[1]->value);
      this->children[0]->setType(t1.type);
      this->children[2]->setType(t2.type);
    }
  }
  else if (this->value == "term")
  {
    if (this->children.size() == 1)
    {
      // term factor
      t = this->children[0]->getexprType(vars);
    }
    else if (this->children.size() == 3)
    {
      // term term STAR factor
      // term term SLASH factor
      // term term PCT factor
      struct type t1, t2;
      t1 = this->children[0]->getexprType(vars);
      t2 = this->children[2]->getexprType(vars);
      t = typeOp(t1, t2, this->children[1]->value);
      this->children[0]->setType(t1.type);
      this->children[2]->setType(t2.type);
    }
  }
  else if (this->value == "factor")
  {
    if (this->children.size() == 1)
    {
      // factor ID
      if (this->children[0]->value == "ID" &&
          this->children[0]->children.size() >= 1)
      {
        if (vars.count(this->children[0]->children[0]->value) == 0)
        {
          tE("ERROR: variable " + this->children[0]->children[0]->value +
             " was not declared in current scope.");
        }
        else if (vars.count(this->children[0]->children[0]->value) != 0)
        {
          t.var = this->children[0]->children[0]->value;
          t.type = vars[this->children[0]->children[0]->value];
        }
      }
      else if (this->children[0]->value == "NUM" &&
               this->children[0]->children.size() >= 1)
      {
        // factor NUM
        t.var = this->children[0]->children[0]->value;
        t.type = INT;
      }
      else if (this->children[0]->value == "NULL" &&
               this->children[0]->children.size() >= 1)
      {
        // factor NULL
        t.var = "NULL";
        t.type = INTS;
      }
    }
    else if (this->children.size() == 2)
    {
      if (this->children[0]->value == "AMP")
      {
        // factor AMP lvalue
        if (this->children[1]->getexprType(vars).type == INT)
        {
          t.type = INTS;
        }
        else
        {
          tE("ERROR: Pointer to a pointer is not supported.");
        }
      }
      else
      {
        // factor STAR factor
        if (this->children[1]->getexprType(vars).type == INTS)
        {
          t.type = INT;
        }
        else
        {
          tE("ERROR: INT cannot be dereferenced.");
        }
      }
    }
    else if (this->children.size() == 3)
    {
      if (this->children[0]->value == "LPAREN")
      {
        // factor LPAREN expr RPAREN
        t = this->children[1]->getexprType(vars);
      }
      else if (this->children[0]->value == "ID" &&
               this->children[0]->children.size() >= 1)
      {
        // factor ID LPAREN RPAREN (function call)

        // but if variable with the same name was initialized
        if (vars.count(this->children[0]->children[0]->value) != 0)
        {
          tE("ERROR: variable " + this->children[0]->children[0]->value +
             " is not a function.");
        }
        t.type = INT;
      }
    }
    else if (this->children[0]->value == "ID" && this->children.size() == 4 &&
             this->children[0]->children.size() >= 1)
    {
      // factor ID LPAREN arglist RPAREN (function call)

      // but if variable with the same name was initialized
      if (vars.count(this->children[0]->children[0]->value) != 0)
      {
        tE("ERROR: variable " + this->children[0]->children[0]->value +
           " is not a function.");
      }
      t.type = INT;
    }
    else if (this->children.size() == 5)
    {
      // factor NEW INT LBRACK expr RBRACK
      if (this->children[3]->getexprType(vars).type == INT)
      {
        t.type = INTS;
      }
      else
      {
        tE("ERROR: Argument of new int[] must be an integer.");
      }
    }
  }
  else if (this->value == "lvalue")
  {
    if (this->children.size() == 1)
    {
      // lvalue ID
      if (this->children[0]->value == "ID" &&
          this->children[0]->children.size() >= 1)
      {
        if (vars.count(this->children[0]->children[0]->value) == 0)
        {
          tE("ERROR: variable " + this->children[0]->children[0]->value +
             " was not declared in current scope.");
        }
        else if (vars.count(this->children[0]->children[0]->value) != 0)
        {
          t.var = this->children[0]->children[0]->value;
          t.type = vars[this->children[0]->children[0]->value];
        }
      }
    }
    else if (this->children.size() == 2)
    {
      // lvalue STAR factor
      if (this->children[1]->getexprType(vars).type == INTS)
      {
        t.type = INT;
      }
      else
      {
        tE("ERROR: INT cannot be dereferenced.");
      }
    }
    else if (this->children.size() == 3)
    {
      // lvalue LPAREN lvalue RPAREN
      t = this->children[1]->getexprType(vars);
    }
  }
  return t;
}

// ---------------- Exception functions implementations -----------------

ScanningFailure::ScanningFailure(std::string message)
    : message(std::move(message)) {}

const std::string &ScanningFailure::what() const { return message; }

// ----------------------------- helper func implementation
// ------------------------
std::string mipsWord(std::string label) { return ".word " + label; }
std::string branchLabel(std::string rule, int s, int t, std::string label)
{
  return (rule == "beq")
             ? "beq $" + std::to_string(s) + ", $" + std::to_string(t) + ", " +
                   label
             : "bne $" + std::to_string(s) + ", $" + std::to_string(t) + ", " +
                   label;
}
std::string mipsCodeFac(std::string instrID, int a, int b, int c,
                        std::string comment, int i)
{
  std::string code;
  if (comment == noComment)
  {
    comment = "";
  }
  else
  {
    comment = " \t; " + comment;
  }

  switch (getOpcode(instrID))
  {
  // -------- format-1 -------- //
  case jr:
  case jalr:
    if (a == -1)
    {
      // should not happen, it's hard coded.
      tE("ERROR: " + instrID + "'s parameter is wrong, fix this right now!");
    }
    code = instrID + " $" + std::to_string(a) + comment;
    break;
  // -------- format-2 -------- //
  case add:
  case sub:
  case slt:
  case sltu:
    if (a == -1 || b == -1 || c == -1)
    {
      // should not happen, it's hard coded.
      tE("ERROR: " + instrID + "'s parameter is wrong, fix this right now!");
    }
    code = instrID + " $" + std::to_string(a) + ", $" + std::to_string(b) +
           ", $" + std::to_string(c) + comment;
    break;
  // -------- format-3 -------- //
  case beq:
  case bne:
    if (a == -1 || b == -1)
    {
      // should not happen, it's hard coded.
      tE("ERROR: " + instrID + "'s parameter is wrong, fix this right now!");
    }
    code = instrID + " $" + std::to_string(a) + ", $" + std::to_string(b) +
           ", " + std::to_string(i) + comment;
    break;
  // -------- format-4 -------- //
  case lis:
  case mflo:
  case mfhi:
    if (a == -1)
    {
      // should not happen, it's hard coded.
      tE("ERROR: " + instrID + "'s parameter is wrong, fix this right now!");
    }
    code = instrID + " $" + std::to_string(a) + comment;
    break;
  // -------- format-5 -------- //
  case mult:
  case multu:
  case divt:
  case divu:
    if (a == -1 || b == -1)
    {
      // should not happen, it's hard coded.
      tE("ERROR: " + instrID + "'s parameter is wrong, fix this right now!");
    }
    code = instrID + " $" + std::to_string(a) + ", $" + std::to_string(b) +
           comment;
    break;
  // -------- format-6 -------- //
  case sw:
  case lw:
    if (a == -1 || b == -1)
    {
      // should not happen, it's hard coded.
      tE("ERROR: " + instrID + "'s parameter is wrong, fix this right now!");
    }
    code = instrID + " $" + std::to_string(a) + ", " + std::to_string(i) +
           "($" + std::to_string(b) + ") " + comment;
    break;
  // -------- format-7 (.word) -------- //
  case word:
    code = ".word " + std::to_string(i) + comment;
    break;
  // -------- format-undefined -------- //
  case notfound:
  default:
    // should never happend
    tE("ERROR: Opcode unrecgonized, found: " + instrID);
  }
  return code;
}
void appendSV(std::vector<std::string> &a, std::vector<std::string> &b)
{
  a.insert(std::end(a), std::begin(b), std::end(b));
}

void addAssignment(std::vector<std::string> &source, int d, int value,
                   std::string comment)
{
  if (value == 1)
  {
    source.push_back(mipsCodeFac("add", d, 11, 0, "Assignment"));
  }
  else if (value == 4)
  {
    source.push_back(mipsCodeFac("add", d, 4, 0, "Assignment"));
  }
  else if (value == 8)
  {
    source.push_back(mipsCodeFac("add", d, 18, 0, "Assignment"));
  }
  else if (value == 12)
  {
    source.push_back(mipsCodeFac("add", d, 12, 0, "Assignment"));
  }
  else
  {
    source.push_back(mipsCodeFac("lis", d, -1, -1, comment, -1));
    source.push_back(mipsCodeFac("word", -1, -1, -1, noComment, value));
  }
}
void addStore(std::vector<std::string> &source, int s, int d, int index)
{
  source.push_back(
      mipsCodeFac("sw", s, d, -1, "push variables onto frame stack", index));
}
void addLoad(std::vector<std::string> &source, int s, int d, int index)
{
  source.push_back(
      mipsCodeFac("lw", s, d, -1, "pop variables onto frame stack", index));
}
void addInitFP(std::vector<std::string> &source, int size, std::string proc)
{
  addStore(source, 31, 30, -4);
  source.push_back(mipsCodeFac("sub", 29, 30, 18, "init fp", -1));
  addAssignment(source, 6, size * 4, "change main stack pointer");
  source.push_back(mipsCodeFac("sub", 30, 29, 6, "frame", -1));
}
void addPush(std::vector<std::string> &source)
{
  addStore(source, 3, 30, -4);
  source.push_back(mipsCodeFac("sub", 30, 30, 4, "push", -1));
}
void addPop(std::vector<std::string> &source)
{
  source.push_back(mipsCodeFac("add", 30, 30, 4, "pop", -1));
  addLoad(source, 5, 30, -4);
}

void funcCall(std::vector<std::string> &source, std::string func)
{
  if (func != "init")
  {
    source.push_back(mipsCodeFac("add", 8, 0, 1, "tmp for $1"));
    source.push_back(" ");
    source.push_back(mipsCodeFac("add", 1, 3, 0, "  copy to $1, for function call", -1));
    source.push_back(mipsCodeFac("lis", 10, -1, -1, "$10 <-- func addr", -1));
    source.push_back(mipsWord(func));
    source.push_back(mipsCodeFac("jalr", 10, -1, -1, "call subroutine", -1));
    source.push_back(" ");
    source.push_back(mipsCodeFac("add", 1, 0, 8, "back for $1"));
  }
  else
  {
    source.push_back(mipsCodeFac("add", 8, 0, 31, "tmp for $31"));
    source.push_back(mipsCodeFac("lis", 10, -1, -1, "$10 <-- func addr", -1));
    source.push_back(mipsWord(func));
    source.push_back(mipsCodeFac("jalr", 10, -1, -1, "call subroutine", -1));
    source.push_back(mipsCodeFac("add", 31, 0, 8, "back for $31"));
  }
}

void fCall(std::vector<std::string> &source, std::string func)
{
  // push 31 and 29 and 5 into stack
  addStore(source, 31, 30, -4);
  addStore(source, 29, 30, -8);
  addStore(source, 5, 30, -12);
  source.push_back(mipsCodeFac("sub", 30, 30, 12));
  source.push_back(mipsCodeFac("lis", 10, -1, -1, "$10 <-- func addr", -1));
  source.push_back(mipsWord(func));
  source.push_back(mipsCodeFac("jalr", 10, -1, -1, "call subroutine", -1));
  source.push_back(mipsCodeFac("add", 30, 30, 12));
  addLoad(source, 31, 30, -4);
  addLoad(source, 29, 30, -8);
  addLoad(source, 5, 30, -12);
}

void initMain(std::vector<std::string> &source)
{
  source.push_back("lis $11             ;; $11 = 1");
  source.push_back(".word 1");
  source.push_back("lis $4              ;; $4 = 4");
  source.push_back(".word 4");
  source.push_back("add $18, $4, $4     ;; $18 = 8");
  source.push_back("add $12, $4, $18     ;; $12 = 12");
}

// if two nodes are the same
bool sameTree(tree &node1, tree &node2)
{
  bool result = false;

  return result;
}

// if two nodes are constants
struct consFold areConstants(tree &node1, tree &node2)
{
  struct consFold cons;

  cons.isConstant = false;
  bool consYes = false;

  int num1 = 0, num2 = 0;

  if (node1.value == "expr")
  {
    if (node2.value == "term")
    {
      if (node2.children[0]->children.size() > 0 && node2.children[0]->children[0]->value == "factor" && node2.children[0]->children[0]->children.size() > 0 && node2.children[0]->children[0]->children[0]->value == "NUM" && node2.children[0]->children[0]->children[0]->children.size() > 0)
      {
        if (node1.children.size() > 0 && node1.children[0]->value == "term" && node1.children[0]->children.size() > 0 && node1.children[0]->children[0]->value == "factor" && node1.children[0]->children[0]->children.size() > 0 && node1.children[0]->children[0]->children[0]->value == "NUM" && node1.children[0]->children[0]->children[0]->children.size() > 0)
        {
          num2 = std::stoi(node2.children[0]->children[0]->children[0]->value);
          num1 = std::stoi(node1.children[0]->children[0]->children[0]->children[0]->value);
          consYes = true;
        }
      }
    }
  }
  else if (node1.value == "term")
  {
    if (node2.value == "factor")
    {
      if (node2.children[0]->children[0]->children.size() > 0 && node2.children[0]->children[0]->children[0]->value == "NUM" && node2.children[0]->children[0]->children[0]->children.size() > 0)
      {
        if (node1.children[0]->children.size() > 0 && node1.children[0]->children[0]->value == "factor" && node1.children[0]->children[0]->children.size() > 0 && node1.children[0]->children[0]->children[0]->value == "NUM" && node1.children[0]->children[0]->children[0]->children.size() > 0)
        {
          num2 = std::stoi(node2.children[0]->children[0]->value);
          num1 = std::stoi(node1.children[0]->children[0]->children[0]->value);
          consYes = true;
        }
      }
    }
  }

  if (consYes == true)
  {
    cons.isConstant = true;

    cons.plus = num1 + num2;
    cons.minus = num1 - num2;
    cons.star = num1 * num2;
    if (num2 != 0)
    {
      cons.slash = num1 / num2;
      cons.mod = num1 % num2;
    }
  }

  return cons;
}