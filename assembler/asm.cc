#include "scanner.h"
#include <climits>
#include <iostream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>


class Parser
{
private:
  std::unordered_map<std::string, uint32_t> labelTable; // <"labels":"address">

  enum opcode
  {
    notfound = 0,
    jr,
    jalr,
    add,
    sub,
    slt,
    sltu,
    beq,
    bne,
    lis,
    mflo,
    mfhi,
    mult,
    multu,
    div,
    divu,
    sw,
    lw
  };

public:
  Parser() {}

  void outputInstr(uint32_t const instr);

  void translateAndOutput(std::vector<std::vector<Token>> const &tokenLineList);

  opcode getOpcode(std::string const &inString);

  uint32_t translator(std::vector<Token> const &tokenList, int64_t const programCounter);
  uint32_t translateWord(std::vector<Token> const &tokenList);
  uint32_t translateOpcode(std::vector<Token> const &tokenList, int64_t const programCounter);

  void findLabels(std::vector<std::vector<Token>> &tokenLineList, std::vector<std::vector<Token>> &desLineList);
  void printLabelTable();

  bool labelExist(std::string const &label);
  uint32_t getLabelAddress(std::string const &label);
  void setLabel(std::string const &label, uint32_t const address);
};

// tE wrapps the function to throw an exception
void tE(std::string exception)
{
  throw ScanningFailure(exception);
}

////////////////////////////////
//     assembler function     //
////////////////////////////////
int assem()
{
  std::string line;
  Parser p;
  try
  {
    std::vector<std::vector<Token>> tokenLineListInput;
    std::vector<std::vector<Token>> tokenLineListModified;
    while (getline(std::cin, line))
    {
      std::vector<Token> tokenLine = scan(line);
      // take the instructions and store
      if (tokenLine.size() != 0)
      {
        tokenLineListInput.push_back(tokenLine);
      }
    }

    // record each label
    p.findLabels(tokenLineListInput, tokenLineListModified);

    // translate the instruction
    p.translateAndOutput(tokenLineListModified);

    // output the labelTable
    // p.printLabelTable();
  }
  catch (ScanningFailure &f)
  {
    std::cerr << f.what() << std::endl;
    return 1;
  }
  return 0;
}

///////////////////////////
//     main function     //
///////////////////////////
int main()
{
  return assem();
}

// outputInstr will output the instruction
void Parser::outputInstr(uint32_t const instr)
{
  std::cout << char(instr >> 24) << char(instr >> 16) << char(instr >> 8) << char(instr);
  return;
}

// translate is a wrapper for translator
void Parser::translateAndOutput(std::vector<std::vector<Token>> const &tokenLineList)
{
  if (tokenLineList.size() == 0)
  {
    return;
  }
  // translate the instruction
  int64_t pc = 1;
  for (std::vector<Token> tokenLine : tokenLineList)
  {
    outputInstr(translator(tokenLine, pc));
    ++pc;
  }
}

// translator is a switch distribute translate work to each individual function
uint32_t Parser::translator(std::vector<Token> const &tokenList, int64_t const programCounter)
{
  uint32_t instr;
  switch (tokenList.at(0).getKind())
  {
  case Token::ID:
    instr = translateOpcode(tokenList, programCounter);
    break;
  case Token::WORD:
    instr = translateWord(tokenList);
    break;
  case Token::LABEL:
    // should have been erased in the first pass
  case Token::COMMA:
  case Token::LPAREN:
  case Token::RPAREN:
  case Token::INT:
  case Token::HEXINT:
  case Token::REG:
  case Token::WHITESPACE:
  case Token::COMMENT:
    tE("ERROR:  Expecting opcode, label, or directive, but got: " + tokenList.at(0).getLexeme());
    break;
  default:
    tE("ERROR: DOTID token unrecognized: " + tokenList.at(0).getLexeme());
  }
  return instr;
}

// translateWord will translate .word instruction
uint32_t Parser::translateWord(std::vector<Token> const &tokenList)
{
  // .word only accept 2 tokens
  if (tokenList.size() != 2)
  {
    tE("ERROR: format for .word is invalid. need \".word <paramter>\" parameter can only be INT, HEXINT, or LABEL");
  }

  // check the second token
  Token token = tokenList.at(1);
  int64_t instr = token.toLong();
  switch (token.getKind())
  {
  case Token::INT:
    // check boundry
    if (instr > ((int64_t(1) << 32) - 1) || instr < INT_MIN)
    {
      tE("ERROR: the value is out of the range, should be in [-2^31,2^32-1], but found: " + token.getLexeme());
    }
    break;
  case Token::HEXINT:
    // check boundry
    if (instr > 0xffffffff)
    {
      tE("ERROR: the value is out of the range, should be in less than 0xffffffff, but found: " + token.getLexeme());
    }
    break;
  case Token::ID:
    // look for the address in the symbol table
    {
      std::string label = token.getLexeme();
      if (labelExist(label))
      {
        instr = getLabelAddress(label);
      }
      else
      {
        tE("ERROR: this label:" + label + " cannot be found");
      }
      break;
    }
  default:
    tE("ERROR: .word format unrecgonized, found: " + token.getLexeme());
  }
  return instr & 0xffffffff;
}

// translateOpcode will translate the instructions that have opcodes in them
uint32_t Parser::translateOpcode(std::vector<Token> const &tokenList, int64_t const programCounter)
{
  uint32_t instr = 0, opCodeLf = 0, opCodeRi = 0;
  int64_t rS = 0, rT = 0, rD = 0, imme = 0;
  bool useImme = 0, opcodeSet = 0;
  std::string instrID = tokenList.at(0).getLexeme();

  auto invalidForm = [](std::string op) {
    tE("ERROR: Wrong format for instruction: \"" + op + "\"");
  };

  // set the parameters and check format
  switch (getOpcode(instrID))
  {
  case notfound:
    tE("ERROR: Opcode unrecgonized, found: " + instrID);
    break;
  // -------- format-1 -------- //
  case jr:
    opCodeRi = 8;
    opcodeSet = 1;
  case jalr:
    opCodeRi = (opcodeSet) ? opCodeRi : 9;
    // check format
    if (tokenList.size() == 2 && tokenList.at(1).getKind() == Token::REG)
    {
      // assign parameters
      rS = tokenList.at(1).toLong();
      break;
    }
    invalidForm(instrID);
  // -------- format-2 -------- //
  case add:
    opCodeRi = 32;
    opcodeSet = 1;
  case sub:
    opCodeRi = (opcodeSet) ? opCodeRi : 34;
    opcodeSet = 1;
  case slt:
    opCodeRi = (opcodeSet) ? opCodeRi : 42;
    opcodeSet = 1;
  case sltu:
    opCodeRi = (opcodeSet) ? opCodeRi : 43;
    // check format
    if (tokenList.size() == 6 && tokenList.at(1).getKind() == Token::REG &&
        tokenList.at(2).getKind() == Token::COMMA && tokenList.at(3).getKind() == Token::REG &&
        tokenList.at(4).getKind() == Token::COMMA && tokenList.at(5).getKind() == Token::REG)
    {
      // assign parameters
      rD = tokenList.at(1).toLong();
      rS = tokenList.at(3).toLong();
      rT = tokenList.at(5).toLong();
      break;
    }
    invalidForm(instrID);
  // -------- format-3 -------- //
  case beq:
    opCodeLf = 4;
    opcodeSet = 1;
  case bne:
    opCodeLf = (opcodeSet) ? opCodeLf : 5;
    // check format
    if (tokenList.size() == 6 && tokenList.at(1).getKind() == Token::REG &&
        tokenList.at(2).getKind() == Token::COMMA && tokenList.at(3).getKind() == Token::REG &&
        tokenList.at(4).getKind() == Token::COMMA)
    {
      useImme = 1;
      // assign parameters
      rS = tokenList.at(1).toLong();
      rT = tokenList.at(3).toLong();

      switch (tokenList.at(5).getKind())
      {
      case Token::INT:
        imme = tokenList.at(5).toLong();
        if (imme > 32767 || imme < -32768)
        {
          tE("ERROR: parameters for the instruction are invalid [out-of-range], INT should be in [-2^15 , 2^15-1]");
        }
        break;
      case Token::HEXINT:
        imme = tokenList.at(5).toLong();
        if (imme > 0xffff)
        {
          tE("ERROR: parameters for the instruction are invalid [out-of-range], HEXINT should be less than or equal to 0xffff");
        }
        break;
      case Token::ID:
      {
        std::string label = tokenList.at(5).getLexeme();
        if (labelExist(label))
        {
          imme = (int64_t(getLabelAddress(label)) / 4) - programCounter;
          if (imme > 32767 || imme < -32768)
          {
            tE("ERROR: parameters for the instruction are invalid [out-of-range], INT should be in [-2^15 , 2^15-1]");
          }
        }
        else
        {
          tE("ERROR: No such label: " + label);
        }
        break;
      }
      default:
        invalidForm(instrID);
      }
      break;
    }
    invalidForm(instrID);
  // -------- format-4 -------- //
  case lis:
    opCodeRi = 20;
    opcodeSet = 1;
  case mflo:
    opCodeRi = (opcodeSet) ? opCodeRi : 18;
    opcodeSet = 1;
  case mfhi:
    opCodeRi = (opcodeSet) ? opCodeRi : 16;
    // check format
    if (tokenList.size() == 2 && tokenList.at(1).getKind() == Token::REG)
    {
      // assign parameters
      rD = tokenList.at(1).toLong();
      break;
    }
    invalidForm(instrID);
  // -------- format-5 -------- //
  case mult:
    opCodeRi = 24;
    opcodeSet = 1;
  case multu:
    opCodeRi = (opcodeSet) ? opCodeRi : 25;
    opcodeSet = 1;
  case div:
    opCodeRi = (opcodeSet) ? opCodeRi : 26;
    opcodeSet = 1;
  case divu:
    opCodeRi = (opcodeSet) ? opCodeRi : 27;
    // check format
    if (tokenList.size() == 4 && tokenList.at(1).getKind() == Token::REG &&
        tokenList.at(2).getKind() == Token::COMMA && tokenList.at(3).getKind() == Token::REG)
    {
      // assign parameters
      rS = tokenList.at(1).toLong();
      rT = tokenList.at(3).toLong();
      break;
    }

    invalidForm(instrID);
  // -------- format-6 -------- //
  case sw:
    opCodeLf = 43;
    opcodeSet = 1;
  case lw:
    opCodeLf = (opcodeSet) ? opCodeLf : 35;
    // check format
    if (tokenList.size() == 7 && tokenList.at(1).getKind() == Token::REG &&
        tokenList.at(2).getKind() == Token::COMMA && tokenList.at(5).getKind() == Token::REG &&
        tokenList.at(4).getKind() == Token::LPAREN && tokenList.at(6).getKind() == Token::RPAREN)
    {
      useImme = 1;
      // assign parameters
      switch (tokenList.at(3).getKind())
      {
      case Token::INT:
        imme = tokenList.at(3).toLong();
        if (imme > 32767 || imme < -32768)
        {
          tE("ERROR: parameters for the instruction are invalid [out-of-range], INT should be in [-2^15 , 2^15-1]");
        }
        break;
      case Token::HEXINT:
        imme = tokenList.at(3).toLong();
        if (imme > 0xffff)
        {
          tE("ERROR: parameters for the instruction are invalid [out-of-range], HEXINT should be less than 0xffff");
        }
        break;
      case Token::ID:
        tE("ERROR: Label not allowed in sw or lw");
      default:
        invalidForm(instrID);
      }

      rT = tokenList.at(1).toLong();
      rS = tokenList.at(5).toLong();
      break;
    }
    invalidForm(instrID);
  // -------- format-undefined -------- //
  default:
    tE("ERROR: Opcode unrecgonized, found: " + instrID);
  }

  // check boundary
  if (rS > 31 || rT > 31 || rD > 31 || rS < 0 || rT < 0 || rD < 0)
  {
    tE("ERROR: There is no such register [out-of-range], only 32 registers available [0,31]");
  }

  // combine the instruction
  if (useImme)
  {
    instr = (opCodeLf & 0x3f) << 26 | (uint16_t(rS) & 0x1f) << 21 | (uint16_t(rT) & 0x1f) << 16 | (imme & 0xffff);
  }
  else
  {
    instr = uint16_t(rS) << 21 | (uint16_t(rT) & 0x1f) << 16 | (uint16_t(rD) & 0x1f) << 11 | (opCodeRi & 0x3f);
  }

  return instr;
}

// findLabels iterate through the instructions and record the labels, returns a modified instruction storage
void Parser::findLabels(std::vector<std::vector<Token>> &tokenLineList, std::vector<std::vector<Token>> &desLineList)
{
  uint32_t pc = 0;
  for (std::vector<Token> &tokenLine : tokenLineList)
  {
    while (tokenLine.size() >= 1 && tokenLine.at(0).getKind() == Token::LABEL)
    {
      std::string label = tokenLine.at(0).getLexeme();
      label.pop_back();
      // if label exists already
      if (labelExist(label))
      {
        tE("ERROR: found multiple labels of: " + label);
      }
      setLabel(label, pc);
      tokenLine.erase(tokenLine.begin());
    }
    if (tokenLine.size() != 0)
    {
      desLineList.push_back(tokenLine);
      pc += 4;
    }
  }
  return;
}

// printLabelTable will print all the labels and their addresses
void Parser::printLabelTable()
{
  for (std::pair<std::string, uint32_t> label : labelTable)
  {
    std::cerr << label.first << ' ' << label.second << std::endl;
  }
}

// labelExist returns 1 if the label exists in the table
bool Parser::labelExist(std::string const &label)
{
  return labelTable.find(label) == labelTable.end() ? 0 : 1;
}

// getLabelAddress returns the address of a label (need to do labelExist first to make sure)
uint32_t Parser::getLabelAddress(std::string const &label)
{
  return labelTable.find(label)->second;
}

// setLabel inserts into labelTable
void Parser::setLabel(std::string const &label, uint32_t const address)
{
  labelTable[label] = address;
}

// getOpcode returns the enum for opcodes
Parser::opcode Parser::getOpcode(std::string const &inString)
{
  if (inString == "jr")
    return Parser::jr;
  if (inString == "jalr")
    return Parser::jalr;
  if (inString == "add")
    return Parser::add;
  if (inString == "sub")
    return Parser::sub;
  if (inString == "slt")
    return Parser::slt;
  if (inString == "sltu")
    return Parser::sltu;
  if (inString == "beq")
    return Parser::beq;
  if (inString == "bne")
    return Parser::bne;
  if (inString == "lis")
    return Parser::lis;
  if (inString == "mflo")
    return Parser::mflo;
  if (inString == "mfhi")
    return Parser::mfhi;
  if (inString == "mult")
    return Parser::mult;
  if (inString == "multu")
    return Parser::multu;
  if (inString == "div")
    return Parser::div;
  if (inString == "divu")
    return Parser::divu;
  if (inString == "sw")
    return Parser::sw;
  if (inString == "lw")
    return Parser::lw;
  return Parser::notfound;
}
