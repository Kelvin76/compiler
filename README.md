# Intro

This repository includes the code that I wrote for a compiler project.

## About the compiler

* The whole project includes a compiler and an assembler
* The compiler translates a C-like language into MIPS32 (assembly)
* The assembler translates MIPS into machine code (binary)
* All the code is written in C++
* Includes some optimization of code generation (Won first place in the contest on file size out of 400 students!)
* Does not include linking

## compiling process

### compiler
1, Receive a source file written in a high level language (a C-like language)

&nbsp;&nbsp;&nbsp;&nbsp;&dArr;&nbsp;&dArr;


2, Lexer/Scanner
- Using maximal munch to scan all the tokens in the source file 
- Report lexical error if an invalid input is found

&nbsp;&nbsp;&nbsp;&nbsp;&dArr;&nbsp;&dArr;

3, Parser
- Using lr1 and cfg to test if the given source file could be generated from the laguage rules
- Report error when the test failed

&nbsp;&nbsp;&nbsp;&nbsp;&dArr;&nbsp;&dArr;

 4, Semantic analysis
- Check if all the variable names and function names are declared before calling
- Check if the types of variables and functions (return type) are used correctly when calling

&nbsp;&nbsp;&nbsp;&nbsp;&dArr;&nbsp;&dArr;

5, Code generation and optimization
- Generating MIPS code based on several conventions (e.g., the use of some registers are fixed)
- Optimize the MIPS code based on some techniques (e.g., make efficient use of all registers)

&nbsp;&nbsp;&nbsp;&nbsp;&dArr;&nbsp;&dArr;

### assembler
6, Translate MIPS (assembly) code into binary code 
- Using lexer (the same technique used in the compiler) to get all the tokens
- Generating binary code from the tokens by using the MIPS reference table